# ReRAM

## Source Enviornment Variables

Multiple enviornment variables are referenced as paths from your home directory to load libraries from the Skywater PDK. To ensure they are the correct path for YOUR directory, run the following commands in terminal from the root of the repository after activating the toolchain with `~/toolchain/activate`.

NOTE: It is required that the SkyWater130B PDK is used for ReRAM, to use the proper ReRAM cells. Skywater130A PDK is NOT supported for ReRAM at this time. 

```bash
export PDKPATH=$(pwd)/dependencies/pdks/sky130B ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK_ROOT=$(pwd)/dependencies/pdks

```

## ReRAM Symbol Update

An updated copy of the ReRAM symbol has been made so that it can be simulated with NGSpice: https://github.com/barakhoffer/sky130_ngspice_reram

To update Sky130B libray with new ReRAM symbol:
1. Download ReRAM symbol from Git: https://github.com/barakhoffer/sky130_ngspice_reram/blob/main/xschem/sky130_fd_pr/reram_cell.sym
2. Copy new ReRAM symbol in Sky130B xschem cell library: `dependencies/pdks/sky130B/libs.text/xschem/sky130_fd_pr`

NOTE: NGSpice requires OSDI support to be simulated. This is currently supported by the toolchain. 

## XSchem

To start xschem:
1. Navigate to xschem directory: `cd xschem`
2. Start xschem: `xschem`

Verify the top level Skywater schematic from the PDK, `top.sch` is loaded on startup. If so, you have sourced the xschem tech file properly with the above source command. 

Navigate to the `tb_reram` testbench:
1. On the left-hand side, under the analog primitive section, look for a box labeled `tb_reram`
2. While selecting primitive, press `e` to open testbench

## Running Testbench

To run simulation:
1. Select `netlist` in top right corner
2. Select `simulate` in top right corner

To load the waveforms for the simulation, press `ctrl+left click` ont he `load waves` box on the testbench. 