# Analog Wrapper

The analog user wrapper is the top level schematic and layout files that are referenced to insert your design into, which will be fabricated. Similar to the design process to create a schematic, layout, and run LVS, this can be repeated within the context of the analog wrapper design. 

The following files can be referenced to insert your design into:
- Schematic: `/xschem/user_analog_project_wrapper.sch`
- Layout: `/mag/user_analog_project_wrapper_empty.mag`
- Netgen template: `/netgen/run_lvs_wrapper_xschem.sh`

## Source Enviornment Variables

Multiple enviornment variables are referenced as paths from your home directory to load libraries from the Skywater PDK. To ensure they are the correct path for YOUR directory, run the following commands in terminal from the root of the repository after activating the toolchain with `~/toolchain/activate`.

```bash
export PDKPATH=$(pwd)/dependencies/pdks/sky130A ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

### Schematic

To begin, open xschem inside the `xschem` directory again:
1. `cd xschem`
2. `xschem`

Once started, open the file `user_analog_project_wrapper`. This serves as the top level module to insert all user designs. This shows how the inverter schematic symbol is inserted. The input Vin is hooked up to logic analyzer bit 0, and the output is hooked up to logic analyzer bit 1. 

Once again, enter `netlist` in the top right to generate the `user_analog_project_wrapper` spice file. Note, there will be many errors due to undriven pins, but this is okay. 

### Layout

Next, the design must be inserted in the layout using Magic, inside of the same analog wrapper. A provided caravel file under the `/mag` directory is included, named `user_analog_project_wrapper.mag`. This file was edited to include the inverter layout file. 

Start Magic:
1. Navigate to the `mag` directory: `cd mag`
2. Open magic: `magic -rcfile $PDKPATH/libs.tech/magic/sky130A.magicrc`

Open the magic file `user_analog_wrapper_empty.mag`.

The contents of encapsulated cells, such as the inverter, can be viewed by pressing `x` in magic while selecting the inverter with `s`. 

The logic analyzer pins for Vin and Vout are on the metal 2 layer (pink), and the power pins for VDD and VSS are on the metal 3 layer (gray). 

To extract user analog wrapper from Magic:
1. `extract all`
2. `ext2spice lvs`
3. `ext2spice`

### LVS check

Navigate to netgen folder: `cd netgen`

Source the LVS check: `source run_lvs_wrapper_xschem.sh`

Verify that the schematic and layout SPICE netlists match, and the netgen console reports `Circuits match uniquely`