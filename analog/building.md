# Building Analog Caravel User Project

## Caravel Repo Build

The Caravel project uses a Makefile to install required dependencies such as the top level Caravel design and docker references. Run through the following steps to setup the project, to enable simulations and layout in the Caravel framework. To install the project, it must be ran through the `comparch` host server which is located in the Durham 310 lab. 

This can be SSHed into for remote work: [Connect to `comparch` via `ssh`](/intro/new_members?id=connecting-to-a-new-machine)

1. SSH into comparch host: `ssh comparch`
2. Clone any analog repo: `git clone https://git.ece.iastate.edu/isu-chip-fab/analog_tutorials.git`
3. CD into repo: `cd caravel_user_project_analog`
4. Verify repository populated: `ls`
5. Activate Toolchain: `~/toolchain/activate`
    - This will use Docker for build
6. Build Repo: `make setup`

## Repository Heirarchy

The following top level directories serve the following purpose: 
- `xschem`: Schematics and testbenches using SkyWater standard cells from XSchem
- `mag`: Generated layout of cells from Magic
- `netgen`: LVS Output from comparing spice schematics and magic layout using `netgen`
- `verilog`: Behavioral digital Verilog code for circuits and top level analog wrapper
- `gds`: Output total hardened macro to be fabricated

## VDI Toolflow Setup

All of the required tools come packaged with an in house toolchain. This includes required tools such as Magic and XSchem. To use these package tools on an Iowa State Linux instance and a GUI interface, the `Linux Engineering` VDI instances can be used. The steps below outline how to install both the VDI and toolchain to your Linux home directory. 

NOTE: After the project is built with the above steps, all work can be done through the Linux VDI instance after sourcing the toolchain. 

1. Login to `Engineering Linux` VDI Instance
2. Open Terminal instance
3. [Install toolchain](/intro/toolchain?id=running-on-vdi-analog-design-or-physical-machine)

To activate toolchain: `~/toolchain/activate`

To exit toolchain, while active: `exit`

NOTE: The toolchain MUST be activated to use any required tools. If something does not run, ensure the toolchain is activated first. 
