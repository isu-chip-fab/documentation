# Post Layout Simulations

Post layout simulations can be achieved by extracting a spice file from the inverter layout, which can be used to ensure that the simulation still performs as expected. 

Reference Document: https://sddec23-08.sd.ece.iastate.edu/reports/Senior%20Design%20Enviornment_ReRAM%20Setup.pdf

## Source Enviornment Variables

Multiple enviornment variables are referenced as paths from your home directory to load libraries from the Skywater PDK. To ensure they are the correct path for YOUR directory, run the following commands in terminal from the root of the `analog_tutorials` repository after activating the toolchain with `~/toolchain/activate`.

```bash
export PDKPATH=$(pwd)/dependencies/pdks/sky130A ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Parasitic Netlist Extraction

Start Magic:
1. Navigate to the `mag` directory: `cd mag`
2. Open magic: `magic -rcfile $PDKPATH/libs.tech/magic/sky130A.magicrc`

Open the target `.mag` layout file desired to be extracted.

Run the following commands in the Magic window to generate extraction. It is reccomended to make a copy of your magic layout file before modifying. The original file name is referenced as `DESIGN` in the steps below, and should be renamed with the original magic file. 

Commands to flatten: 
- select top cell
- expand
- flatten DESIGN_Flat
- load DESIGN_Flat
- cellname delete DESIGN
- cellname rename DESIGN_Flat DESIGN
- select top cell
- extract do local
- extract all
- ext2sim labels on
- ext2sim
- extresist tolerance 10
- extresist
- ext2spice lvs
- ext2spice cthresh 0
- ext2spice extresist on
- ext2spice

With this, a flattened version of the layout has been created. The commands `extresist tolerance` and `ext2spice cthresh` set the bottom threshold of the resistance/capacitances that will be extracted. The generated spice file will be a combination of resistory and capacitor components. 

## Post-Layout Simulation

Next, the extracted design can be imported to an XSchem design. 

First begin by opening the generated symbol file that was created previously for a circuit design. Start by opening `xschem` under the folder `/xschem`.

The following actions must be done to update your original testbench to insert the post layout spice file:
1. In the design under test, edit the symbol field `type=subcircuit` to `type=primitive`. 
    - This will ensure the spice file is read directly and not from the original schematic
2. Add `value=".include /path/to/layout.spice"` with the path to your new spice file in the transient control block for your testbench
3. Regenerate the netlist for the top level testbench by selecting `Netlist` in XSchem
4. Open the newly generated testbench spice file, and reorder the pins so that they match the layout SPICE file

For point 4, as an example, if the original two files report the nets for the unit under test in order as:
- Layout.spice: Vin VDD VSS Vout
- testbench.spice: VDD Vin Vout GND

The testbench pins for the device under test should be reordered as:
- Layout.spice: Vin VDD VSS Vout
- testbench.spice: Vin VDD GND Vout

Note that `GND` SHOULD NOT be renamed to `VSS`, since this net corresponds to what is labeled in the testbebench, and correlates to the shared ground with the voltage sources. Only the order should be changed, since `VSS` in the layout corresponds to `GND` in the testbench. When this is complete, make sure to save the testbench spice file.

Next, a simulation can be run by selecting `Simulate` in the top right of XSchem. 

After the simulation is complete, an output waveform file can be saved with `write DEVICE.raw all` in the `ngspice` terminal that appears when the simulation starts. 

The waveform viewer `gaw` can be opened by selecting `Waves`. Note that the same configuration steps as above apply here. 

To load the saved waveforms, in `gaw`, select `File`, `Open`, and navigate to the saved `DEVICE.raw` in your file heirarchy. 

The same steps can be used to verify the functionality of the inverter as the previous simulation steps. 
