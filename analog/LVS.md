# LVS Checks

Reference Document: https://sddec23-08.sd.ece.iastate.edu/reports/Senior%20Design%20Enviornment_ReRAM%20Setup.pdf

Netgen documentation: http://opencircuitdesign.com/netgen/index.html

## Source Enviornment Variables

Multiple enviornment variables are referenced as paths from your home directory to load libraries from the Skywater PDK. To ensure they are the correct path for YOUR directory, run the following commands in terminal from the root of the repository after activating the toolchain with `~/toolchain/activate`.

```bash
export PDKPATH=$(pwd)/dependencies/pdks/sky130A ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Running LVS

`Netgen` is used for Layout Versus Schematic (LVS) checks to compare spice circuits from `XSchem` and the generated spice layout of the circuit from `Magic`. 

Sample shell commands are given under `/netgen/` for running LVS checks between two different generated spice models. This can be used to verify that the spice models generated from netgen and xschem match. 

The analog caravel project has a sample shell script which calls one `netgen lvs` command, format as follows:
`netgen lvs “schematic.spice schematic_subcircuit_name” “magic.spice magic_subcircuit_name” setup_file.tcl`

To run an LVS check with the shell commands:
1. Navigate to netgen folder: `cd netgen`
2. Source shell netgen lvs command: `source run_DESIGN.sh`

An output of the LVS will be generated under `/netgen/comp.out`. A popup window will come up and report `Circuits match uniquely` if LVS passes.
