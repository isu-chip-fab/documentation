# Schematic Simulations

Reference Document: https://sddec23-08.sd.ece.iastate.edu/reports/Senior%20Design%20Enviornment_ReRAM%20Setup.pdf

## Testbench Design

`/xschem_library/devices`
- `vsource.sym`: Voltage source
- `gnd.sym`: Ground pin
- `lab_pin.sym`: attaches to nets and provides name for testbench waveforms

A pulse wave can be generated as follows. 

`pulse(VL VH TD TR TF PW PER PHASE)`
- VL: Lower voltage
- VH: Higher voltage
- TD: Delay
- TF: Fall time
- TR: Rise time
- PW: Pulse width
- PER: Period
- Phase: Phase shift

A square wave can be created with the command `value=pulse(0 1.8 1ns 1ns 1ns 5ns 10ns)` for a voltage source. 

A corner test is also required, which can be added from the `sky130_fd_pr` directory, with the name `corner.sym`.

A transient can be set to determine how long the simulation should run, with `value=".tran .01n 1u .save all"`. This transient will run from 0 to 1us in 0.01ns steps. It also will save all waveform data. 

To navigate down into a symbol, you can press `ctrl+e`. 

## Running Simulation

To Configure GAW Simulator (Preferred from ReRAM 1 senior design team): 
1. Select `Simulation` from top menu, then `Configure simulators and tools`
2. Check the box next to `Ngspice batch` instead of `Ngspice`
3. Check the box for `Gaw viewer` as well
4. Select `accept and close` for settings to take effect

To run simulation:
1. Select `netlist` in top right corner
2. Select `simulate` in top right corner

After the simulation is done running, a window will appear and report completion. 

## Verification

The waveform viewer `Gaw` is used to view waveforms, which is also supported in the toolchain. 

To View Waveforms:
1. select `waves` in the top right corner
2. Drag the signal names from the left window to the black windows in `gaw`
3. Select different colors for signals by right clicking on the signal names and selecting `Change Color...`
4. Scale the window of the waveform with `Z In` and `Z Out` on the top menu
