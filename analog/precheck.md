## Running Precheck

The Caravel Precheck will run multiple different tests such as DRC and LVS to ensure the design will pass fabrication. 

All Make commands are run at the root of the caravel repository.

To clone the MPW Precheck repository: `make precheck`

To run the MPW Precheck: `make run-precheck`