# Layout

Reference Document: https://sddec23-08.sd.ece.iastate.edu/reports/Senior%20Design%20Enviornment_ReRAM%20Setup.pdf

## Reference

Magic tutorial: http://opencircuitdesign.com/magic/

Magic Cheat-Sheet: https://github.com/iic-jku/osic-multitool/blob/main/magic-cheatsheet/magic_cheatsheet.pdf

Useful walkthrough video of layout design in Magic from eFabless: https://www.youtube.com/watch?v=XvBpqKwzrFY

## Source Enviornment Variables

Multiple enviornment variables are referenced as paths from your home directory to load libraries from the Skywater PDK. To ensure they are the correct path for YOUR directory, run the following commands in terminal from the root of the repository after activating the toolchain with `~/toolchain/activate`.

```bash
export PDKPATH=$(pwd)/dependencies/pdks/sky130A ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Using Magic

Start Magic:
1. Navigate to the `mag` directory: `cd mag`
2. Open magic: `magic -rcfile $PDKPATH/libs.tech/magic/sky130A.magicrc`

NOTE: The `-rcfile` option points to a required library file which is stored in the dependencies of the skywater PDK relative to the analaog tutorials repo. 

To verify that the correct rc library file was loaded, confirm that in the top brown bar of the Magic GUI that `Technology: sky130A` is present. You should also be able to read multiplke different layers on the right side of the Magic GUI. 

To open a magic file:
1. Select `File`, then `Open`
2. Select `MODULE.mag`, then `Open`

## Design Rule Check (DRC)

DRC is required to verify that the layout design will match all constraints for the eFabless skywater130 fabrication process. If DRC fails, the module and design will not be able to be submitted or fabricated. 

DRC errors can be checked by monitoring the `DRC` window on the top bar of magic. If the `DRC` window presents a green checkbox, then it means there are no DRC errors present with the sky130 tech files and design. If the `DRC` window is read, you can select `Drc`, `Drc find next error`, to find each DRC error incrementally. The specific error violation will also be displayed in the Magic command terminal to read. 

## Layout Extraction

Layout files can be extracted to generate a SPICE file, which can be compared to the XSchem schematic file to ensure the same netlists match with LVS. 

Run the following commands in the Magic terminal to generate `.ext` and `.spice` files for LVS check:
1. `extract all`
2. `ext2spice lvs`
3. `ext2spice`

## Useful Commands

Useful navigation commands:
- Arrow keys: pan up, down, left, right
- Scroll up/down: pan up, down
- Shift + Scroll: pan left, right
- `v`: Full view
- `z`: Zoom in
- `shift+z`: Zoom out

Useful edit commands:
- `spacebar`: Switch between different edit modes (can view in Command terminal)
- `i`: Select device mouse cursor over
- `x`: Expands cells in layout. 