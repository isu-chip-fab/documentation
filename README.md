# ChipForge Documentation

This wiki is intended primarily for Iowa State University's ASIC design co-curricular **ChipForge**. This includes relevant information for all aspects of ASIC design that ChipForge is involved in, including digital, analog, and validation of chips. There is documentation and support for how to use open source ASIC design tools supported by eFabless, as well as internal tools and resources only accessible by students in ChipForge. This page references multiple sets of tools to streamline the ASIC design process for ChipForge members, including tool dependencies, onboarding material, and design tutorials, as an example. 

## Introduction

If you are new to this page, and starting out with ChipForge, run through the following pages first: [Introduction](intro/index.md)

After this is complete, proceed to the following project specific pages. 

## Project Specific

There are multiple different avenues of design and testing for ChipForge, including bringup testing, digital design, and analog design. Refer to the following sections for the desired work.

Do you want to run code on an ASIC as part of the **bringup** process? [Bringup](bringup/index.md)

Do you want to run through the **digital** design process of an ASIC? [Digital](digital/index.md)

Do you want to run through the **analog** design process of an ASIC? [Analog](analog/index.md)

Are you involved designing or assembling **hardware** that uses an ASIC design? [Hardware](hardawre/index.md)

## Project Standards

Do you need to know more about expected coding and Git procedures? [Standards](standard/index.md)

## ChipForge Maintnence

ChipForge offers a `Meta` section which includes general upkeep for the organization of this wiki. This would be most useful for leadership: [Meta](meta/index.md)

![GDS Blender](/img/README/23-28_GDS.JPG)
