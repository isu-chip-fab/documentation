# Bringup Embedded Firmware Overview

This page is an overview of some of the tips and tricks for embedded programming as well as things to keep in mind when writing tiny programs.

## Linker Scripts

## Standard Library Alternatives

### Where are malloc/free?
In an embedded system, these are not usually needed. There is so little memory on this system, and it is running one single program, that using global variables is usually preferred. A common mistake when using malloc is that it returns 0 (a NULL pointer) when it fails to find enough memory, but often programmers forget to check that and assume the memory will always exist. In an embedded system, the likelihood that malloc will return 0 is much higher than on a desktop, and then you have to figure out what to do when malloc returns 0. Alternatively, if you use a large global array, the compiler will tell you if it is too large and you don't have to figure out what to do at runtime, instead you fix your code before running it.

Another major pitfall here is allocating large arrays on the stack. Stack space (local variables in a function) should be limited. You should never have large arrays as local variables, as your stack space is limited to a small subset of the total memory available. Instead, use global variables for large blocks of memory.

## Assembly

