# Bringup Reference

## Digital Design Intro

### Basic Logic Gates

<script type="text/tikz" data-show-console="true">
\usepackage{circuitikz}
\begin{document}
\begin{circuitikz}
    \draw (1,1) node[and port](A1){} 
          (A1.in 1) node[anchor=east] {A}
          (A1.in 2) node[anchor=east] {B}
          (A1.out) node[anchor=west] {X};

    \draw (4,1) node[or port](O1){}
          (O1.in 1) node[anchor=east] {A}
          (O1.in 2) node[anchor=east] {B}
          (O1.out) node[anchor=west] {X};

    \draw (7,1) node[not port, anchor=out](N1){}
          (N1.in) node[anchor=east] {A}
          (N1.out) node[anchor=west] {X};

    \draw (0,2) node[]{AND};
    \draw (3,2) node[]{OR};
    \draw (6,2) node[]{NOT};

\end{circuitikz}
\end{document}
</script>

Logic gates are the basis for any digital circuit, and are typically designed using MOSFET or BJT transistors. Asynchronous logic gates, such as AND, OR, and NOT gates seen above, occur asynchronously, such that the output changes at any moment one of the inputs change. Logic gates can be combined in parallel and series to create more complex logic structures, or other digital designs such as multiplexors and D Flip Flops. These gates are utilized to design the digital caravel ASIC designs, as well as the RISC-V management core. 

In general, an AND gate only outputs 1 if both inputs are 1. An OR gate outputs 1 if EITHER input is 1. A NOT gate inverts the output from the input logic level. Logic levels are commonly reffered to as either 0 or 1, but this corresponds to an analog voltage level, typically 0V (Ground) and 3.3V or 5V, depending on the input voltage to the system. 

Truth tables for an AND, OR, and NOT gate:

| A | B | AND X | OR X | NOT X |
| - | - | :-:   | :-:  | :-:   |
| 0 | 0 | 0     | 0    | 1     |
| 0 | 1 | 0     | 1    | 1     |
| 1 | 0 | 0     | 1    | 0     |
| 1 | 1 | 1     | 1    | 0     |


### D Flip Flops and Synchronous Design

<script type="text/tikz" data-show-console="true">
\usepackage{circuitikz}
\begin{document}
\begin{circuitikz}
    \node[flipflop D](D1){}
    (D1.pin 3) -- ++(-0.5, 0) node[anchor=east] {CLK};
\end{circuitikz}
\end{document}
</script>

A D Flip Flop is a synchronous digital device which can store data. A synchronous design means it is clocked, and only changes states/data on either the rising (0 to 1) or falling (1 to 0) edge of the clock signal. Between those states, the written or reset data is retained. When the D Flip Flop is reset, it can be asynchronously cleared, so the stored data is set to a known state (typically 0). In between the rising or falling clock edge, the data is retained and cannot change, unless an asynchronous reset is asserted. On the edge of the clock, the D Flip Flop will store the value from the input D. The port nQ is an inverted output of the stored bit Q, which is optionally included in some designs. 

Truth table for rising edge, asynchronous reset, D Flip Flop:

| Clk  | Rst | Q   | nQ     |
| :-:  | :-: | :-: | :-:    |
| x    | 1   | 0   | 1      |
| 0    | 0   | Q   | nQ     |
| 1    | 0   | Q   | nQ     |
| 0->1 | 0   | D   | nD     |


### Shift Register

<script type="text/tikz" data-show-console="true">
\usepackage{circuitikz}
\begin{document}
\begin{circuitikz}
    \draw (3,2) node[flipflop D](D1){};
    \draw (7,2) node[flipflop D](D2){};
    \draw (11,2) node[flipflop D](D3){};

    \draw (D1.pin 1) node[left]{D};

    \draw (D1.pin 6) -- ++(1, 0) node[above]{Q0} -- (D2.pin 1);
    \draw (D2.pin 6) -- ++(1, 0) node[above]{Q1} -- (D3.pin 1);
    \draw (D3.pin 6) -- ++(1, 0) node[above]{Q2};

    \draw (D1.pin 3) -- ++(0, -0.7);
    \draw (D2.pin 3) -- ++(0, -0.7);
    \draw (D3.pin 3) -- ++(0, -0.7) to ++(-8.5, 0) node[left]{Clk};

\end{circuitikz}
\end{document}
</script>

A shift register consists of multiple DFF modules in series, with the output Q of a previous DFF wired to the input data D of the following DFF. Each DFF is hooked up to the same clock, so the values will change synchronously during the same edge of the shared clock. Only 1 bit of data is wired in to the leftmost DFF, which is then propogated on every clock cycle through each DFF. On the next edge of the clock, each data input will write the currently stored data Q from the preceeding DFF. 

Consider the following diagram, where each cycle the data input D is written into the first DFF:

| Cycle # | D   | Q0  | Q1  | Q2  |
| :-:     | :-: | :-: | :-: | :-: |
| 0       | 0   | 0   | 0   | 0   |
| 1       | 1   | 1   | 0   | 0   |
| 2       | 0   | 0   | 1   | 0   |
| 3       | 0   | 0   | 0   | 1   |
| 4       | 0   | 0   | 0   | 0   |

### IO Pins / Tri-State Buffer

<script type="text/tikz" data-show-console="true">
\usepackage{circuitikz}
\begin{document}
\begin{circuitikz}
    
    \draw (0, 2) node[buffer port, scale=-1](A){};
    \draw (0, 0) node[buffer port](B){};

    \draw (A.out) -- ++(-0.5, 0) -- ++(0, -2) -- (B.in 1);

    \draw (A.south){} -- ++(0, -0.3) -- ++(0, 1.3) -- ++(2, 0) node[right]{Dir};
    \draw (A.in) -- ++(1, 0) node[right]{Out};
    \draw (B.out) -- ++(1, 0) node[right]{In};

    \draw (A.out) -- ++(-2, 0) node[left, anchor=south]{In/Out};

\end{circuitikz}
\end{document}
</script>

Tri-state buffers include an enable pin (seen at the top of the component) which act as an enable line. This example utilizes a tri-state buffer to create a GPIO pin, which can be configured to be used as either an input or output to an ASIC. When Dir is 1, the ASIC will output data to the GPIO pin, and output it externally. When Dir is 0, this tri-state output buffer is set to high-impedence, meaning data will not transfer from the ASIC. Instead, the pin will act as an input, where another buffer will input data to go into the ASIC and be read. 

| Dir | Direction |
| :-: | :-:       |
| 0   | input     |
| 1   | Output    |

## Course References
- CPRE 281 Stoychev Slides: https://home.engineering.iastate.edu/alexs/classes/2022_Fall_281/
- CPRE 288 Jones Slides: https://class.ece.iastate.edu/cpre288/lecture.asp
- CPRE 488 Jones Slides: https://class.ece.iastate.edu/cpre488/schedule.asp
