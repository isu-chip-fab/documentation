# Bringup Index

Are you designing/testing Firmware for an ASIC?
1. [Tutorial - Start Here](tutorial/index.md)

Are you testing hardware for an ASIC?
1. [Hardware Guide](hw_guide.md)

Supplemental Information:
- [Firmware](firmware.md)
- [Reference](reference.md)
- [Q&A](QA.md)