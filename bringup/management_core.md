# Bringup Management Core Overview

The current revision of Management Core in MPW-7 uses [Litex](https://github.com/enjoy-digital/litex) to generate a RISC-V CPU core.

Reference [https://caravel-mgmt-soc-litex.readthedocs.io/en/latest](https://caravel-mgmt-soc-litex.readthedocs.io/en/latest) and the `firmware/chipignite/_docs/caravel_registers.pdf` file in the `caravel_board` repo for auto-generated documentation including register lists.

The base CPU used in the management core is [VexRiscv](https://github.com/SpinalHDL/VexRiscv/tree/b6118e5cc2a33323425df6455697139021d50c72?tab=readme-ov-file).

For more information on RISC-V processor cores, see the RISC-V processor specification at [https://riscv.org/technical/specifications/](https://riscv.org/technical/specifications/), Volume 1 and Volume 2.

This core consists of
* 1.5KiB of RAM
* External SPI Flash (4MiB)
* UART @ 9600 baud
* Debug UART @ 115200 baud (disabled by default, enables GDB debugging?)
* Timer (for semi-accurate timing)
* Housekeeping SPI (direct access to memory and user area)
* Logic Analyzer pins (internal connections between management core and the user area)
* Management IO pins (external connections shared between management core and user area)
* External GPIO pin (LED)

## Memory Map
```
Start      | End        | Length     | Name  | Description

0x00000000 | 0x000003ff | 0x00000400 | dff   | D Flip Flop Memory
0x00000400 | 0x000005ff | 0x00000200 | dff2  | D Flip Flop Memory 2
0x10000000 | 0x10ffffff | 0x01000000 | flash | External Flash Memory
0x30000000 | 0x3fffffff | 0x10000000 | mprj  | User Project
0x26000000 | 0x262fffff | 0x00300000 | hk    | HouseKeeping Register Space
0xf0000000 | 0xf000ffff | 0x00010000 | csr   | SoC Control Register Space
```

## Control Register Map
```
Start      | Name         | Description

0xf0000000 | ctrl         | Control
0xf0000800 | debug_mode   | GDB Debug Mode ?
0xf0001000 | debug_oeb    | GDB Debug Mode ?
0xf0001800 | flash_core   | SPI Flash Config
0xf0002800 | gpio         | Single GPIO LED
0xf0003000 | la           | LA Probes
0xf0003800 | mprj_wb_iena | ????
0xf0004000 | spi_enabled  | SPI Enable
0xf0004800 | spi          | SPI Master
0xf0005000 | timer0       | Timer
0xf0005800 | uart         | UART Peripheral
0xf0006000 | uart_enabled | UART Enable
0xf0006800 | user_irq0    | User Interrupt 0
0xf0007000 | user_irq1    | User Interrupt 1
0xf0007800 | user_irq2    | User Interrupt 2
0xf0008000 | user_irq3    | User Interrupt 3
0xf0008800 | user_irq4    | User Interrupt 4
0xf0009000 | user_irq5    | User Interrupt 5
0xf0009800 | user_irq_ena | User Interrupt Enable
```

## GPIO (LED)

Blinky!

https://skywater-pdk.readthedocs.io/en/main/contents/libraries/sky130_fd_io/docs/user_guide.html#sky130-fd-io-gpiov2-additional-features

```
 ien | oe  | mode0 | mode1 |  INPUT  | OUTPUT

  0  |  0  |   0   |   0   |    -    |   -
  0  |  0  |   0   |   1   |    -    |   -
  0  |  0  |   1   |   0   |    -    |   -
  0  |  0  |   1   |   1   |    -    |   -
  0  |  1  |   0   |   0   |    -    |   -
  0  |  1  |   0   |   1   |    -    | ENABLED
  0  |  1  |   1   |   0   |    -    |   -
  0  |  1  |   1   |   1   |    -    | PULL (5k)
  1  |  0  |   0   |   0   |    -    |   -
  1  |  0  |   0   |   1   | ENABLED |   -
  1  |  0  |   1   |   0   | ENABLED |   -
  1  |  0  |   1   |   1   | ENABLED |   -
  1  |  1  |   0   |   0   |    -    |   -
  1  |  1  |   0   |   1   | ENABLED | ENABLED
  1  |  1  |   1   |   0   | ENABLED |   -
  1  |  1  |   1   |   1   | ENABLED | PULL (5k)
```



## Interrupts

WIP Notes: mtvec CSR is _write-only_, in contradiction to the RISC-V specification which states it must be at minimum read-only.

See [https://github.com/litex-hub/pythondata-cpu-vexriscv/blob/master/pythondata_cpu_vexriscv/verilog/src/main/scala/vexriscv/GenCoreDefault.scala](https://github.com/litex-hub/pythondata-cpu-vexriscv/blob/master/pythondata_cpu_vexriscv/verilog/src/main/scala/vexriscv/GenCoreDefault.scala). The minimal+debug configuration is the one we are using.

Interrupt | Module
---|---
0 | TIMER0
1 | UART
2 | USER_IRQ_0
3 | USER_IRQ_1
4 | USER_IRQ_2
5 | USER_IRQ_3
6 | USER_IRQ_4
7 | USER_IRQ_5


## SPI
