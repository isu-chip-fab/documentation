# Hardware Guide

The purpose of this page is to overview how to verify the hardware of the Caravel Development Board and provide setup for software validation. 

## Breakout Board
Each packaged ASIC is produced on a QFN 50 package. This packaged die is placed on the Caravel Breakout Board, which includes decoupling capacitors and a M.2 pinout connection, intended to be connected to the Caravel Development board. 

![Breakout Board](img/bringup_guide/breakout_board.jpg)

## Development Board

The Caravel Development board contains the following features:
- Reset button
- USB to micro-B programming port
- 38 GPIO ports
- External Reset
- External power connections
- M.2 Edge connector for Caravel Breakout Board
- Jumper connectors to determine power sources

The Caravel Development Board contains two 24 pin connectors on both sides of the board, which
includes 38 GPIO pins, power connections, and an external clock connection. These GPIO pins
can be used to interface with an ASIC design for validation.

![Development Board Features](img/bringup_guide/development_board_features.jpg)

### IO Connections
- The clock is driven by X1 with a frequency of 10MHz. To drive the clock with custom
frequency, disable X1 through installing J6 and use the external pin for xclk
- The voltage regulator U5 and U6 supply 1.8V and 3.3V through J8 and J9. The traces have to
be cut if they are supplied externally.
- vccd1 is connected to 1.8V through J3. The trace has to be cut if it is supplied externally
- vddio is connected to 3.3V through J5. The trace has to be cut if it is supplied externally

![Development Board IO](img/bringup_guide/development_board_IO.jpg)


## Hardware Testing
Test Procedure:
1. Set multimeter to Continuity mode and ensure there are no short circuits between 3.3V and GND
2. Set multimeter to Continuity mode and ensure there are no short circuits between 1.8V and GND
3. Insert USB to Micro-B connector into both the users laptop and the J1 connector on the top of the Evaluation board to supply power
4. Set multimeter to DC Voltage mode and verify J8 reads 1.8V between J8 and GND
5. Set multimeter to DC Voltage mode and verify J9 reads 3.3V between J9 and GND

