# Bringup Guide

The purpose of this page is to overview how to program compiled C programs onto the Caravel Development Board.

**This is old and out of date. See [Tutorial - Start Here](tutorial/index.md) instead**

## Software Setup
1. Install the Caravel Breakout Board onto the Evaluation Board
    - Remove the screw (M1) to add/remove the breakout board module
    - Insert the Caravel Breakout Board into a ZIF connector at a 45 degree angle
    - Press the Caravel Breakout Board down so that it is flush with the Development Board, and turn the screw (M1) down
2. Connect IO pins for respective design
    - NOTE: This is dependent on the design under test
3. Insert USB to Micro-B connector into both the users laptop and the J1 connector on the top
of the Evaluation board

![Breakout Board Installation](/img/bringup_guide/breakout_board_installation.jpg)

## SSH Into Linux Machine 

Using VSCode:
1. In VSCode, Press `F1` on your keyboard, and type `Remote-SSH: Connect to Host`
2. Select the desired SSH host `comparch`
3. Select `Linux` as the desired platform 
4. Select `Open Folder` to open home directory of Linux space

Using Command Line/Terminal
1. Open command line or bash shell of personal machine
2. SSH into Linux machine by entering ssh host name from new members page: `ssh comparch`

*NOTE: The ISU VPN can be utilized to SSH into the Linux machine off-campus*

## Clone Caravel Board Repository to Linux SSH

1. Clone the caravel_board repo to desired directory: `git clone git@git.ece.iastate.edu:isu-chip-fab/caravel_board.git`
    - NOTE: If you DO NOT have a SSH key set up with your Gitlab account, you will have to run the https clone and provide netid login information: `git clone https://git.ece.iastate.edu/isu-chip-fab/caravel_board.git`
2. Navigate to root of newly cloned caravel_board repo with `cd path/to/caravel_board`
3. Enter `ls` to verify contents of caravel_board repository cloned

## Configure tools in Linux SSH

A set of needed dependencies are not installed globally at the moment. 

For now, run `/local/toolchain/activate` in a terminal to create a new shell with the necessary dependencies.

To exit this new shell, run `exit` once.

## Running the Makefile

1. Navigate to root of newly cloned caravel_board repo with `cd path/to/caravel_board`
2. Navigate to the desired firmware folder with code: `cd firmware/chipignite/blink`
3. Run Makfile to program board with folders contents `make clean flash`
    - `clean` make command will delete artifacts of previous build 
    - `flash` will program compiled design over USB 

## Updating Makefiles

The Makefile for each software program in the caravel_board repository will require a few modifications to run on the Linux machines. If you are running a new program this will need to be done!

- Comment out Line 3 with a `#`
- Set Line 9 to `TOOLCHAIN_PREFIX=riscv64-linux-gnu` 
- Comment out Line 11 
- Uncomment Line 12 
- Line 24, insert ` -Wl,--build-id=none` 

## Resources

Caravel Board Git Repo: https://github.com/efabless/caravel_board