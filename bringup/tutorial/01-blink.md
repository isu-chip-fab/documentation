# Tutorial 01 - Blinky!

Welcome to the first introductory tutorial for the ChipForge Bringup!

This assumes you already have [VSCode set up to develop on comparch](/intro/new_members.md?id=connecting-vscode-to-comparch) and have set up SSH keys between your computer and comparch and from comparch to git (all in the same document).

## Clone the Caravel Tutorial Project 
Open VSCode on comparch, and create a new terminal by clicking `Terminal > New Terminal` in the menu bar.

In the terminal, run the following (To `run` a command means to type it as shown and press enter):
```sh
mkdir -p Caravel
cd Caravel
git clone git@git.ece.iastate.edu:isu-chip-fab/caravel_user_project.git --recursive
code caravel_user_project
```

When you open a new terminal in `comparch`, your terminal starts in your home directory which is your `X:` drive. 

1. `mkdir -p Caravel` - Create a folder named Caravel in the "current working directory" (cwd). Since you just logged in, your cwd is your `X:` drive which on linux is accessible at `/home/netid`, so this creates a folder at `X:\Caravel` (Windows) or `/home/netid/Caravel` (Linux). The `pwd` command will "Print the Working Directory" if you want to check where you are.
2. `cd Caravel` - Change directory into the Caravel folder you just made, your cwd is now `/home/netid/Caravel`.
3. `git clone git@git.ece.iastate.edu:isu-chip-fab/caravel_user_project` - Clone, or make a local copy, of the Git repository at [https://git.ece.iastate.edu/isu-chip-fab/caravel_user_project](https://git.ece.iastate.edu/isu-chip-fab/caravel_user_project). Open that link in a browser and find the blue `Code` button and note that the `Clone with SSH` URL shown there matches `git@git.ece.iastate.edu:isu-chip-fab/caravel_user_project_tutorials`. This is how you clone a repository via SSH. This repository uses git submodules, so it must be cloned with the `--recursive` flag.
4. `code caravel_user_project` - When using VSCode's terminal, VSCode adds a command named `code` which opens a folder in VSCode. This opens the folder `caravel_user_project` you just cloned from the ECpE gitlab server in a VSCode window.

At this point, you should see a bunch of files in a tree on the left of the current VSCode window. If you encounter any errors, revisit the [New Members Guide](/intro/new_members.md) for connection issues, or reach out on the ChipForge Microsoft Teams for help.

## Run the first blinky!

In the VSCode terminal on comparch, run the following:
```sh
source /local/toolchain/activate
make setup
make verify-blink-flash@library/blink
```

If you get prompted for which board to use, use one of the `HKSpiPico (ISU Chip Fab Caravel PMOD Breakout)` lines. If this does not appear as an option, reach out on the ChipForge Microsoft Teams for help.

1. `source /local/toolchain/activate` - Some of the necessary tools to build are not installed globally. It is necessary to run this at least once in every terminal before using many of the commands which involve compiling, flashing, or simulating. If you have issues with commands not being found, ensure this file is sourced. See [Toolchain](/intro/toolchain.md).
2. `make setup` - This only needs to be run on a new clone of a Caravel project _once_ or on a major Caravel version change. This clones down several other repos including the `caravel` and `mgmt_core_wrapper`. This must be run before doing most other `make` targets or `make` will fail to find dependencies it needs.
3. `make verify-blink-flash@library/blink` - This instructs the `make` command to compile the `blink` project from `library/blink` and flash it onto a board connected to the `comparch` machine. There are several other `verify-*-*` targets you will use later. Any `verify-*-*@*` target will look for a corresponding design verification test. So the `verify-blink-flash@library/blink` target looks in the `library/blink` folder for the `blink` project, compiles it, then flashes it to an attached physical board.

## Connecting to the webcam (optional)

If you have physical access to the Durham 310 lab, look at the board (one of the green PMOD breakout boads), and an LED should be blinking! If you do not have physical access to a board, we have a webcam set up in Durham 310 you can access to view the physical boards.

To connect to the webcam, open a browser to [http://xilinxcam-6a.ece.iastate.edu](http://xilinxcam-6a.ece.iastate.edu) and login using `chipfab` and password `chipfab`.


## Code Explanation

Now that you have an LED blinking, let's look at how this worked.

The `library/blink/dv/blink` folder contains several files. The verilog can be ignored for now as they are not used when flashing a physical board.
1. `Makefile` - This is the makefile which compiles this design verification test. This is a template which must be copied into all dv test folders.
2. `blink.c` - The embedded firmware program which should be compiled to run on the board.

Opening `blink.c` will show a very short C program:
```c
#include <defs.h>
#include <stub.h>

void main() {
    // Set GPIO output
    reg_gpio_mode1 = 1;
    reg_gpio_mode0 = 0;
    reg_gpio_ien   = 1;
    reg_gpio_oe    = 1;

    // blink 10 times
    for (int i = 0; i < 10; i++) {
        reg_gpio_out   = 1; // LED on
        delay_ms(1);

        reg_gpio_out   = 0; // LED off
        delay_ms(1);
    }
}
```

* `defs.h` and `stub.h` are library files specific to the VexRISCV processor used in this version of the Caravel project. These are located in `library/firmware-vexriscv/verilog/dv/firmware`. `defs.h` provides the register macros (eg. `reg_gpio_mode1`), and `stub.h` provides some useful helper functions (eg. `delay_ms`).
* Note that `main` returns void. If you are used to non-embedded C, this may seem unusual because `main` usually returns an `int`. However, in an embedded system, there is nothing else that runs after main (except an infinite while loop, see the end of `library/firmware_vexriscv/verilog/dv/firmware/crt0_vex.S`, which calls `main`), there is no reason to return a value.

In this context, a `register` is a location in memory that does something special. You will learn more about this in [03 Adder](./03-adder.md). If you look in `defs.h`, you will see that these `reg_*` are actually macros:

```c
#define CSR_BASE 0xf0000000L
#define CSR_GPIO_MODE1_ADDR (CSR_BASE + 0x2800L)
#define IOREG(addr)     (*(volatile uint32_t *) (addr))
#define reg_gpio_mode1  IOREG(CSR_GPIO_MODE1_ADDR)
```

So when you write `reg_gpio_mode1 = 1`, the preprocessor will substitute those macros and you end up with:
```c
(*(volatile uint32_t *) (0xf0000000L + 0x2800L)) = 1;
```

When you usually create a variable and write to it, the variable is assigned an address by the compiler (or via `malloc`). In this case, the physical hardware we are interacting with requires us to write to a specific location in memory. In this case, if we write a `1` to the memory address `0xf0002800`, then something special will happen in hardware.

If you are not used to pointer magic in C, this takes the number `0xf0000000L + 0x2800L = 0xf0002800L`, tells the compiler to treat it as a pointer to a uint32_t (an unsigned 32-bit integer), then writes a 1 to that memory location. Using these macros make this look like a simple variable assignment rather than complex pointer magic.

Configuring this GPIO is not straightforward, this is the table of the four configuration registers and what they do:

https://skywater-pdk.readthedocs.io/en/main/contents/libraries/sky130_fd_io/docs/user_guide.html#sky130-fd-io-gpiov2-additional-features

```
 ien | oe  | mode0 | mode1 |  INPUT  | OUTPUT

  0  |  0  |   0   |   0   |    -    |   -
  0  |  0  |   0   |   1   |    -    |   -
  0  |  0  |   1   |   0   |    -    |   -
  0  |  0  |   1   |   1   |    -    |   -
  0  |  1  |   0   |   0   |    -    |   -
  0  |  1  |   0   |   1   |    -    | ENABLED
  0  |  1  |   1   |   0   |    -    |   -
  0  |  1  |   1   |   1   |    -    | PULL (5k)
  1  |  0  |   0   |   0   |    -    |   -
  1  |  0  |   0   |   1   | ENABLED |   -
  1  |  0  |   1   |   0   | ENABLED |   -
  1  |  0  |   1   |   1   | ENABLED |   -
  1  |  1  |   0   |   0   |    -    |   -
  1  |  1  |   0   |   1   | ENABLED | ENABLED
  1  |  1  |   1   |   0   | ENABLED |   -
  1  |  1  |   1   |   1   | ENABLED | PULL (5k)
```

So configuring the registers above sets the GPIO to input and output enabled. This means you can read the value of the pin in the `reg_gpio_in` register and write the value of the pin in the `reg_gpio_out` register. So in the infinite loop, you can write `reg_gpio_out = 1` to turn on the GPIO pin (drive it to 3.3V), and `reg_gpio_out = 0` to turn off the GPIO pin (drive it to 0V).

If you look at the `CaravelPmod-sch.pdf` in [https://git-pages.ece.iastate.edu/isu-chip-fab/hardware/](https://git-pages.ece.iastate.edu/isu-chip-fab/hardware/), you will see that the GPIO pin is connected to an LED:

![GPIO LED](assets/01-blink/gpio_led.png)


Add in the `delay_ms` call to sleep for 1 second between toggling the LED and you have a nice blink program!


## Simulation

You can also run this program in simulation without physical hardware. Simulation takes a long time (2min for 25ms of simulated time), but it can be extremely useful debugging hardware issues. Because it takes so long, reduce the `delay_ms` calls to only 1ms each before continuing.


To run the blink simulation, run `make verify-blink-rtl@library/blink`. Give it time, it should take about 2 minutes, more if you have a slower computer or multiple people are using comparch.

You should see output similar to below:
```
           674050000: GPIO OFF
           674850000: GPIO ON
          1783550000: GPIO OFF
          3012750000: GPIO ON
          4015350000: GPIO OFF
          5244550000: GPIO ON
          6247150000: GPIO OFF
          7476350000: GPIO ON
          8478950000: GPIO OFF
          9708150000: GPIO ON
         10710750000: GPIO OFF
         11939950000: GPIO ON
         12942550000: GPIO OFF
         14171750000: GPIO ON
         15174350000: GPIO OFF
         16403550000: GPIO ON
         17406150000: GPIO OFF
         18635350000: GPIO ON
         19637950000: GPIO OFF
         20867150000: GPIO ON
         21869750000: GPIO OFF
         23098950000: GPIO ON
         24101550000: GPIO OFF

Monitor: Timeout, Test (RTL) Failed
```

Notice that the GPIO toggled at roughly 1ms (timestamps are in ps). `delay_ms` is a very rough estimate of milliseconds, good enough for basic timing tasks. At 25ms, the simulation ended, saying that it timed out. By default, simulations will only run for a fixed amount of time, in this case it is configured to stop running at 25ms, or when `simulation_pass` or `simulation_fail` in `stub.h` are called.

This makes use of one more file in the design verification folder, `blink_tb.v`. This file instantiates the entire Caravel project as a design under test, loads in the C program you wrote, and simulates the entire processor in verilog. This will become more clear later when verilog designs are introduced, but for now a few important sections:

```verilog
initial begin
	$dumpfile("blink.vcd");
	$dumpvars(0, blink_tb);

	// Repeat cycles of 1000 clock edges as needed to complete testbench
	repeat (250) begin
		repeat (1000) @(posedge clock);
		// $display("+1000 cycles");
	end
	$display("%c[1;31m",27);
	`ifdef GL
		$display ("Monitor: Timeout, Test (GL) Failed");
	`else
		$display ("Monitor: Timeout, Test (RTL) Failed");
	`endif
	$display("%c[0m",27);
	$finish;
end
```

This portion creates the `vcd` file (Value Change Dump) you will see more soon, but it also manages the timeout value. If you would like the simulation to run longer, change the `250` to something else, eg. `1000` will make the simulation run for 100ms.

```verilog
/** Additional customizations here **/
always @(posedge gpio) $display("%t: GPIO ON", $realtime);
always @(negedge gpio) $display("%t: GPIO OFF", $realtime);
```
At the bottom, this portion watches the GPIO pin and prints the GPIO ON and GPIO OFF messages whenever the gpio pin has a positive edge (0 -> 1) or negative edge (1 -> 0) with the timestamp.


## Simulation - Viewing the VCD file

<!-- If you are on macOS / Linux, or have WSL installed on Windows, you can use X11 forwarding to view the VCD file. Open a new local terminal and run `ssh -X comparch` to ssh with X11 forwarding enabled. Now any GUI applications you open on comparch will appear on your computer. -->

If you are on Windows without WSL, connect to an `ENGR-VLINUX` VDI instance (https://vdi.engineering.iastate.edu) or work at a physical Linux lab computer in Coover. Then, follow the instructions to install the Toolchain on a VDI or lab machine: [Toolchain VDI Install](../../intro/toolchain.md). Ensure you have activated the toolchain (`~/toolchain/activate`).

In the VDI/physical lab machine, ***NOT IN VSCODE REMOTE-SSH*** run `gtkwave verilog\dv\blink\RTL-blink.vcd` to open the VCD file. A GTKWave window should appear.

In the left sidebar, click on `blink_tb`, and at the bottom of the sidebar, click `gpio` and `Append`. The GPIO waveform should be added to the waveform viewer. Click the Zoom Fit button in the toolbar to zoom out, you should see a GPIO wire toggling up and down approximately once per millisecond.

To see the clock with the gpio pin, click the `clock` wire in the signal list and add it to the viewer. Then you can zoom in and see the clock is running at 10MHz.

![GTKWave](assets/01-blink/gtkwave.png)

This VCD file contains every signal in the entire chip, so if you dig around, you can find the PC address, fetch logic, memory busses, etc.

## FPGA

To run this on the FPGA, first run `make chipforge` to install the chipforge utility. Also, make sure you have brought the delays back to a reasonable amount (1000ms)

Then, run `make verify-blink-fpga@library/blink`. This will create a folder `library/blink/verilog/dv/blink_fpga`, generate a Vivado project, and program the FPGA with the current example project. Once that succeeds and `write_bitstream` finishes, you can run `make verify-blink-flash@library/blink` again and this time select the FPGA. One of the LEDs on the FPGA should now blink (if you just followed the simulation instructions, be sure to set the delays back to longer than 1ms, or you won't see it blink). This is how to program any Caravel project onto an FPGA.

## Exercises

Here are some additional challenges for you to attempt to get a better feel for how this works.

1. Make the LED blink in a `heartbeat` pattern, on for 100ms, off for 100ms, on for 100ms, and off for 700ms repeating.
2. Write a program which takes a single character and makes the LED blink that character in morse code. Can you make it take a string and display the entire string in morse code?
    - Note: This seems like an arbitrary exercise, but many projects may only have an LED to communicate and if that's all you have to debug, you can print error messages and get debug information when an error occurs by reading the morse code on an LED. Eg. [https://github.com/blackmagic-debug/blackmagic/blob/main/src/morse.c](https://github.com/blackmagic-debug/blackmagic/blob/main/src/morse.c).
3. Make the LED appear dim by flashing it on and off very quickly, the longer it stays on compared to how long it stays off determines how bright it appears. This is known as controlling and LED via PWM [https://learn.sparkfun.com/tutorials/pulse-width-modulation/all](https://learn.sparkfun.com/tutorials/pulse-width-modulation/all). Can you make it smoothly dim and brighten repeatedly? (Note: Due to aliasing, this may look very strange through the webcam).
