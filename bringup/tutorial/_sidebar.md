<!-- /sidebar.md -->

[Tutorials](index.md)

* [01 - Blinky!](01-blink.md)
* [02 - UART](02-uart.md)
* [03 - Adder](03-adder.md)
* [04 - Bitbang 7-Seg](04-7bitbang.md)
* [05 - Verilog 7-Seg](05-7verilog.md)
* [06 - RGB LEDs](06-rgb.md)
* [Extra Exercises](extra.md)
* [TODO](todo.md)