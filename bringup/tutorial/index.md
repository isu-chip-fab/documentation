# Bringup Tutorials!

Welcome to the bringup tutorial zone!

If you have not, please first complete [New Member Setup](/intro/new_members.md) instructions. You should be able to `ssh` into the comparch machine, and you should have an SSH key configured with the gitlab server (https://git.ece.iastate.edu).

The sidebar contains several incrementing tutorials to walk you through getting started with this project. Start with [01 - Blinky!](01-blink.md), and continue through the list.

There are also some additional challenges in [Extra Challenges](extra.md) for you to try if you have complete the rest.