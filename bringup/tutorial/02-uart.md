# Tutorial 02 - UART

## Introduction
UART is a bidirectional communication protocol which allows two devices to send and receive data at the same time. In our simple use case, it consists of three connections: RX (receive), TX (transmit), and GND (ground). When connecting two devices A and B, you will always connect A's RX to B's TX and vice-versa so that one device receives what the other transmits. Therefore, if you only need unidirectional data transfer from A to B, you only need to connect A TX to B RX and can ignore B TX to A RX.

The UART protocol is described well [here](https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter).

## Caravel UART Implementation

The Caravel management core's implementation of UART comes from [Litex](https://github.com/enjoy-digital/litex). See [UART Source](https://github.com/enjoy-digital/litex/blob/master/litex/soc/cores/uart.py) for details on how these registers work.

This UART impementation consists of two 16-byte FIFOs (first-in first-out queues). When a byte is received, it will be inserted into the receive FIFO. After 16 bytes, the FIFO will get full and you will start to lose data. The `reg_uart_[rx|tx][full|empty]` registers let you detect the status of these two FIFOS. Attempts to send data when `reg_uart_txfull` is 1 will likely be lost, and attempts to read data when `reg_uart_rxempty` is 1 is [undefined behavior](http://www.catb.org/jargon/html/N/nasal-demons.html).

To send data, you must first enable the UART by writing a 1 to `reg_uart_enable`, then you can immediately send data by writing individual bytes to `reg_uart_rxtx`. Each byte will be sent as soon as possible over the TX pin.

To receive data, the UART must already be enabled, then you must check the value of `reg_uart_rxempty`. If the receive FIFO is empty, there is no data. If it is not empty, then you can read from `reg_uart_rxtx` to get the next byte received. 

*NOTE:* Reading `reg_uart_rxtx` repeatedly will return the same value. Write a 2 to `reg_uart_ev_pending` to clear the rx event and advance the receive FIFO. This is equivalent to telling the UART you are done with the current byte in rxtx and would like to receive another. This should be done after every read. As an additional challenge, see the UART Source link above and see if you can find why this is the case.

## Echo Program
When first using any UART, a good starting place is to make the device echo any data sent to it. This ensures that both directions of communication are functioning.

In the same `caravel_user_project` repository from [Tutorial 01 - Blinky!](01-blink.md), create a new folder called `echo` inside the `library` directory. This folder will be the project we will work on for this tutorial. Inside of the echo folder create the folder `verilog/dv/echo`. Copy over the `Makefile` from `library/blink/verilog/dv/blink` and place it in your new directory. After copying the makefile remove the last line which reads `include $(PWDD)/../firmware/library.makefile`, this line will include the library for echo which we will not be developing. You can now create a file called `echo.c` in `library/echo/verilog/dv/echo` and write the following code:

```c
#include <defs.h>
#include <stub.h>

void configure_io() {
  //  ======= Useful GPIO mode values =============

  //      GPIO_MODE_MGMT_STD_INPUT_NOPULL
  //      GPIO_MODE_MGMT_STD_INPUT_PULLDOWN
  //      GPIO_MODE_MGMT_STD_INPUT_PULLUP
  //      GPIO_MODE_MGMT_STD_OUTPUT
  //      GPIO_MODE_MGMT_STD_BIDIRECTIONAL
  //      GPIO_MODE_MGMT_STD_ANALOG

  //      GPIO_MODE_USER_STD_INPUT_NOPULL
  //      GPIO_MODE_USER_STD_INPUT_PULLDOWN
  //      GPIO_MODE_USER_STD_INPUT_PULLUP
  //      GPIO_MODE_USER_STD_OUTPUT
  //      GPIO_MODE_USER_STD_BIDIRECTIONAL
  //      GPIO_MODE_USER_STD_ANALOG


  //  ======= set each IO to the desired configuration =============

  //  GPIO 0 is turned off to prevent toggling the debug pin; For debug, make this an output and
  //  drive it externally to ground.

  reg_mprj_io_0 = GPIO_MODE_MGMT_STD_ANALOG;

  // Changing configuration for IO[1-4] will interfere with programming flash. if you change them,
  // You may need to hold reset while powering up the board and initiating flash to keep the process
  // configuring these IO from their default values.

  reg_mprj_io_1 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_2 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
  reg_mprj_io_3 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
  reg_mprj_io_4 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;

  // -------------------------------------------

  reg_mprj_io_5  = GPIO_MODE_MGMT_STD_INPUT_NOPULL; // UART Rx
  reg_mprj_io_6  = GPIO_MODE_MGMT_STD_OUTPUT;       // UART Tx
  reg_mprj_io_7  = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_8  = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_9  = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_10 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_11 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_12 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_13 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_14 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_15 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_16 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_17 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_18 = GPIO_MODE_MGMT_STD_OUTPUT;

  reg_mprj_io_19 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_20 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_21 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_22 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_23 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_24 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_25 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_26 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_27 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_28 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_29 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_30 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_31 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_32 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_33 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_34 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_35 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_36 = GPIO_MODE_MGMT_STD_OUTPUT;
  reg_mprj_io_37 = GPIO_MODE_MGMT_STD_OUTPUT;

  // Initiate the serial transfer to configure IO
  reg_mprj_xfer = 1;
  while (reg_mprj_xfer == 1);
}

void main() {
  configure_io();

  reg_uart_enable = 1;

  while (1) {
    while (reg_uart_rxempty);
    reg_uart_rxtx = reg_uart_rxtx + 1;
    reg_uart_ev_pending = 2;
  }
}
```

This code:
1) Configures the IO pins, specifically the UART RX and TX pins.
2) Enables the UART
3) In an infinite loop:
   1) Waits for the receive fifo to not be empty
   2) Reads the next data value and writes it back + 1 (echo the data)
   3) Advances the receive FIFO

## IO Pin Configuration
The blink program previously only used the gpio pin. The Caravel project also contains 38 IO pins which can be controlled by either the management SOC directly (`GPIO_MODE_MGMT_*`) or the user area in hardware (`GPIO_MODE_USER_*`, using the `io_out` and `io_oeb` signals). For both, each pin can be configured to output-only, input-only, bidirectional, and optionally with a builtin pullup or pulldown resistor.

To configure the IO pin mode, you have to write to the appropriate configuration register `reg_mprj_io_*` to write to the temporary configuration buffer, then write `reg_mprj_xfer` to 1 to cause the temporary configuration buffer to take effect. The default IO configurations are set in `verilog/rtl/user_defines.v`, but it is best practice to ignore those when writing most design verification programs and set the configuration of any IO pins used before using them.

Specifically, this program makes use of IO 5 and IO 6 which according to `caravel/verilog/rtl/caravel_core.v` are the UART RX and TX pins:
```
These pins are overlaid on mprj_io space.  They have the function
below when the management processor is in reset, or in the default
configuration.  They are assigned to uses in the user space by the
configuration program running off of the SPI flash.  Note that even
when the user has taken control of these pins, they can be restored
to the original use by setting the resetb pin low.  The SPI pins and
UART pins can be connected directly to an FTDI chip as long as the
FTDI chip sets these lines to high impedence (input function) at
all times except when holding the chip in reset.

JTAG       = mprj_io[0]		(inout)
SDO 	  = mprj_io[1]		(output)
SDI 	  = mprj_io[2]		(input)
CSB 	  = mprj_io[3]		(input)
SCK	  = mprj_io[4]		(input)
ser_rx     = mprj_io[5]		(input)
ser_tx     = mprj_io[6]		(output)
irq 	  = mprj_io[7]		(input)

spi_sck    = mprj_io[32]		(output)
spi_csb    = mprj_io[33]		(output)
spi_sdi    = mprj_io[34]		(input)
spi_sdo    = mprj_io[35]		(output)
flash_io2  = mprj_io[36]		(inout) 
flash_io3  = mprj_io[37]		(inout) 

These pins are reserved for any project that wants to incorporate
its own processor and flash controller.  While a user project can
technically use any available I/O pins for the purpose, these
four pins connect to a pass-through mode from the SPI slave (pins
1-4 above) so that any SPI flash connected to these specific pins
can be accessed through the SPI slave even when the processor is in
reset.

user_flash_csb = mprj_io[8]
user_flash_sck = mprj_io[9]
user_flash_io0 = mprj_io[10]
user_flash_io1 = mprj_io[11]
```

**IMPORTANT NOTE:** _Always_ set IO pins 1-4 as
```c
reg_mprj_io_1 = GPIO_MODE_MGMT_STD_OUTPUT;
reg_mprj_io_2 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
reg_mprj_io_3 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
reg_mprj_io_4 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
```
These are used to communicate to the housekeeping SPI, if these are changed, you will be _unable to reprogram the board_ easily. Be careful setting pin configurations to not change those four.

## Running the echo program
As before, simply run `make verify-echo-flash@library/echo`, assuming you have already run `make setup` previously in this repository and `source /local/toolchain/activate` in the current terminal. The board is ready to echo UART data. 

## Finding the UART
All the development boards contain USB-UART adapters. To find the correct one for your current board, run `chipforge list`. You should look for the entries labeled `HKSpiPico`.

The FPGA uses an external `STM32 USB-SPI Adapter` for communicating to the housekeeping SPI, in this case it is located at `/dev/ttyACM0` (this should look familiar from the list shown when you run `make verify-echo-flash@library/echo`), and its UART port is the `Digilent USB Device - Digilent USB Device` at `/dev/ttyUSB2`. These are independent USB ports, so if more than one FPGA is connected, it may require some trial-and-error to find which Digilent USB device is connected to which USB-SPI adapter. 

The Caravel PMOD breakout board (the green one) uses a Raspberry Pi Pico, if you programmed the board at `/dev/ttyACM4` (`Pico - Pico HKSPI`), then the corresponding UART port is usually `/dev/ttyACM3` (`Pico - Pico UART`). The UART will always come directly before the corresponding HKSPI port.

The eFabless Caravel breakout board (the black one) uses a single RS232-HS for both HKSPI and UART, so it is at `/dev/ttyUSB0`. Note, on all the other boards, the UART can be open and listening while being programmed via the HKSPI. Since the HKSPI and UART share hardware on the eFabless Caravel breakout board, attempts to flash while the UART port is open will fail. Close any serial monitors before flashing the eFabless Caravel breakout board and reopen them after flashing has completed.

## Connecting to the serial port

To connect to the seiral port, you may use pyserial miniterm or `screen`. `screen` tends to be more reliable, but miniterm is easier to use. By default, the management SoC runs at 9600 baud, but `9600` can be replaced with a different baud rate if needed. 

To use screen, run `screen /dev/ttyUSB0 9600`, replacing `/dev/ttyUSB0` with the UART port identified above. Use the three-step sequence `CTRL-A`, release, `K`, release, `Y` to exit.

To use miniterm, run `python3 -m serial.tools.miniterm /dev/ttyUSB0`, replacing `/dev/ttyUSB0` with the UART port identified above. Use `CTRL-]` to exit.

When connected to the UART, type something and it should be appear echoed back on the screen as the next ASCII value (if you type an `a`, you should see a `b` appear). If nothing appears to happen, something is broken or you have the wrong UART port. What happens if you press return? Why does that happen?

Note: The UART is timed based on a reference clock of 10MHz. If you change the input clock from the default 10MHz, the UART will change frequency proportionally.

## Library Functions

If you explored `stub.h` and `stub.c` previously (`library/firmware_vexriscv/verilog/dv/firmware/stub.h`), you may have noticed a few text IO functions available:
```c
void putchar(char c);
char getchar();

void print(const char *p);
void print_hex(uint32_t v, int digits);
void print_dec(uint32_t v);
void print_digit(uint32_t v);
```

These work the same as above, but make writing longer strings to the UART port easier. I encourage you to explore how they are implemented.

A simple echo program may now be:
```c
void main() {
  configure_io();

  reg_uart_enable = 1;

  while (1) {
    putchar(getchar() + 1);
  }
}
```
which does the same as above. These are preferred over using raw register accesses in the future.

## Suggested Challenges

1) Try modifying the characters being sent. Instead of echoing + 1 (which breaks special characters), try capitalizing all letters or applying a simple rotation cipher. See https://en.wikipedia.org/wiki/ASCII#Character_set for a table of all ASCII values, and https://www.dcode.fr/rot-cipher for a simple rotation cipher.
2) Use pyserial to open the serial port to send and receive data using a Python script to the UART port. Then, try sending binary data rather than ascii data, is there any difference?
3) Require the user to enter a passphrase and check if it is correct.
4) Write a simple text-based menu that accepts a frequency and/or pattern from the user to blink the on-board LED.
