# Challenge 05 - RGB LED (Work in Progress)

If you have reached this point, send a message to Gregory Ling to finish writing the tutorials.

## Prerequisites
 * [Install `fpgagen` in `comparch`](/caravel/fpga?id=install-using-pip3)

## Setup
These challenges should all be run on `comparch` physically or via `ssh`.

Clone the tutorials repository and checkout the `simple_rgb` branch on the comparch machine.
Don't forget to run `/local/toolchain/activate` if you have opened a new ssh session.
```
git clone git@git.ece.iastate.edu:isu-chip-fab/caravel_user_project_tutorials
cd caravel_user_project_tutorials
git checkout simple_rgb
make setup
```

Then, with the Arty-A7-100T connected to comparch, run `fpgagen flash`.
This will synthesize the simple RGB project and flash the Arty FPGA.

This also assumes you have already cloned the `caravel_board` repository.

## Now for the challenges
Note: You should not modify the verilog code for any of challenges 1-7.
1) Run a simple blinky on this board, using the blink program in `caravel_board` just to make sure it's working.
2) Using the pin assignments in `Arty-A7-100.xdc`, write a C program to make RGB LED 0 turn red using the `reg_mprj_datal` register using `GPIO_MODE_MGMT_STD_OUTPUT`.
   - You may wish to reference [https://digilent.com/reference/programmable-logic/arty-a7/reference-manual#basic_io](https://digilent.com/reference/programmable-logic/arty-a7/reference-manual#basic_io) for which pins the RGB LEDs are connected to.
3) Using the verilog code as a reference in `verilog/rtl/user_project_wrapper.v` and the pin assignments in `Arty-A7-100.xdc`, write a C program to make RGB LED 0 turn red using `GPIO_MODE_USER_STD_BIDIRECTIONAL`.
4) Extend 3 by making RGB LED 0 white, LED 1 blue, LED 2 green, and LED 3 red.
5) Extend 3 by making RGB LED 0 yellow
6) HARD: Extend 3 by making RGB LED 0 appear orange
   - [https://learn.sparkfun.com/tutorials/pulse-width-modulation/all](https://learn.sparkfun.com/tutorials/pulse-width-modulation/all) is a good reference for PWM.
7) HARD: Extend 3 by making RGB LED 0 cycle through all 24-bit RGB colors (8 bits R, 8 bits G, 8 bits B) in a loop.
8) EXTRA HARD: Create a verilog PWM controller that accepts an 8-bit duty cycle via the wishbone bus, then outputs a PWM with that duty cycle. Add this module to `user_project_wrapper.v` and use this module instead of a software loop to implement 7.
   - This will require adding any verilog files you create to `verilog/includes/includes.rtl.caravel_user_project`.