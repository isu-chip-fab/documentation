# Tutorial 03 - Adder

## Introduction
This tutorial will explore how to interact with custom logic in the user project area. It will also cover how to make use of verilog and C code written as library modules in the chipforge library.

## Verilog Setup

In the same tutorials repository, open `verilog/rtl/user_project_wrapper.v` and replace the contents with the following:

```verilog
`default_nettype none

module user_project_wrapper #(
    parameter BITS = 32
) (
`ifdef USE_POWER_PINS
    inout vdda1,	// User area 1 3.3V supply
    inout vdda2,	// User area 2 3.3V supply
    inout vssa1,	// User area 1 analog ground
    inout vssa2,	// User area 2 analog ground
    inout vccd1,	// User area 1 1.8V supply
    inout vccd2,	// User area 2 1.8v supply
    inout vssd1,	// User area 1 digital ground
    inout vssd2,	// User area 2 digital ground
`endif

    // Wishbone Slave ports (WB MI A)
    input wb_clk_i,
    input wb_rst_i,
    input wbs_stb_i,
    input wbs_cyc_i,
    input wbs_we_i,
    input [3:0] wbs_sel_i,
    input [31:0] wbs_dat_i,
    input [31:0] wbs_adr_i,
    output wbs_ack_o,
    output [31:0] wbs_dat_o,

    // Logic Analyzer Signals
    input  [127:0] la_data_in,
    output [127:0] la_data_out,
    input  [127:0] la_oenb,

    // IOs
    input  [`MPRJ_IO_PADS-1:0] io_in,
    output [`MPRJ_IO_PADS-1:0] io_out,
    output [`MPRJ_IO_PADS-1:0] io_oeb,

    // Analog (direct connection to GPIO pad---use with caution)
    // Note that analog I/O is not available on the 7 lowest-numbered
    // GPIO pads, and so the analog_io indexing is offset from the
    // GPIO indexing by 7 (also upper 2 GPIOs do not have analog_io).
    inout [`MPRJ_IO_PADS-10:0] analog_io,

    // Independent clock (on independent integer divider)
    input   user_clock2,

    // User maskable interrupt signals
    output [2:0] user_irq
);

/*--------------------------------------*/
/* User project is instantiated  here   */
/*--------------------------------------*/

reg [31:0] wbs_dat_s; // Override prior definitions and make these regs.
reg wbs_ack_s;

assign wbs_dat_o = wbs_dat_s;
assign wbs_ack_o = wbs_ack_s;

wire oe_enable_s;

localparam integer NUM_ADDRS = 2;

wire [31:0] wbs_datN_o [NUM_ADDRS:0];
wire wbs_ackN_o [NUM_ADDRS-1:0];
reg wbs_stbN_i [NUM_ADDRS-1:0];

`define address_map(n, address, mask)      \
  wbs_stbN_i[n] <= 0;                      \
  if ((wbs_adr_i & mask) == address) begin \
      wbs_dat_s <= wbs_datN_o[n];          \
      wbs_ack_s <= wbs_ackN_o[n];          \
      wbs_stbN_i[n] <= wbs_stb_i;          \
  end

always @* begin
    wbs_dat_s <= 0;
    wbs_ack_s <= 0;

    // Add entries here to allocate more address ranges
    `address_map(0, 32'h30123400, 32'hFFFFFF00);
    `address_map(1, 32'h30123500, 32'hFFFFFF00);
end



// First wishbone adder
wire [31:0] s_sum1;
wire s_cout1;
wire [31:0] s_a1;
wire [31:0] s_b1;
wire s_cin1;
wire s_wb_enable1;

lib_wishbone_adder adder_0(
    .wb_clk_i(wb_clk_i),
    .wb_rst_i(wb_rst_i),
    .wbs_stb_i(wbs_stbN_i[0]),
    .wbs_cyc_i(wbs_cyc_i),
    .wbs_we_i(wbs_we_i),
    .wbs_sel_i(wbs_sel_i),
    .wbs_dat_i(wbs_dat_i),
    .wbs_adr_i(wbs_adr_i),
    .wbs_ack_o(wbs_ackN_o[0]),
    .wbs_dat_o(wbs_datN_o[0]),

    .o_sum(s_sum1),
    .o_cout(s_cout1),
    .i_a(s_a1),
    .i_b(s_b1),
    .i_cin(s_cin1),
    .i_wb_enable(s_wb_enable1) // 0 - Use i_a, i_b, i_cin; 1 - Use wishbone bus
);


// Second wishbone adder
wire [31:0] s_sum2;
wire s_cout2;
wire [31:0] s_a2;
wire [31:0] s_b2;
wire s_cin2;
wire s_wb_enable2;

lib_wishbone_adder adder_1(
    .wb_clk_i(wb_clk_i),
    .wb_rst_i(wb_rst_i),
    .wbs_stb_i(wbs_stbN_i[1]),
    .wbs_cyc_i(wbs_cyc_i),
    .wbs_we_i(wbs_we_i),
    .wbs_sel_i(wbs_sel_i),
    .wbs_dat_i(wbs_dat_i),
    .wbs_adr_i(wbs_adr_i),
    .wbs_ack_o(wbs_ackN_o[1]),
    .wbs_dat_o(wbs_datN_o[1]),

    .o_sum(s_sum2),
    .o_cout(s_cout2),
    .i_a(s_a2),
    .i_b(s_b2),
    .i_cin(s_cin2),
    .i_wb_enable(s_wb_enable2) // 0 - Use i_a, i_b, i_cin; 1 - Use wishbone bus
);



// LA pin mapping
assign s_a1 = la_data_in[31:0];
assign s_b1 = la_data_in[63:32];
assign s_a2 = la_data_in[63:32];
assign s_b2 = la_data_in[95:64];
assign s_cin1 = la_data_in[96];
assign s_cin2 = la_data_in[97];
assign s_wb_enable1 = la_data_in[98];
assign s_wb_enable2 = la_data_in[99];

assign la_data_out[31:0] = s_sum1;
assign la_data_out[32] = s_cout1;
assign la_data_out[95:64] = s_sum2;
assign la_data_out[96] = s_cout2;

// IO pin mapping
assign oe_enable_s = (s_wb_enable1 || s_wb_enable2); //Output GPIO if wishbone1 and wishbone2 disables

assign io_out[15:0] = s_sum1;
assign io_out[16] = s_cout1;
assign io_oeb[16:0] = {(17){oe_enable_s}};

//Drive undriven outputs
assign la_data_out[63:33] = 0;
assign la_data_out[127:97] = 0;
assign io_out[37:17] = {(21){1'b0}};
assign io_oeb[37:17] = {(21){1'b0}};
assign user_irq = 3'b000;


endmodule	// user_project_wrapper

`default_nettype wire
```

This is a basic Caravel user project that makes use of a 32-bit adder. Do not worry if it seems like a lot at first, it's not expected for this to make complete sense yet. This tutorial is about making use of a user project, not writing one.

The 32-bit adder used above, `lib_wishbone_adder` is defined in the library module (`library/wishbone_adder/verilog/rtl/wishbone_adder.v`). To include a library module in the current project, run `./library/libman use wishbone_adder` to update the appropriate include paths. This will add the appropriate lines to `verilog/includes/includes.rtl.caravel_user_project` to include the `wishbone_adder.v` file from the library module. Check that file and you should now find a reference to the `wishbone_adder.v` file.

Because this design is not fabricated, we cannot just flash code onto one of the breakout boards. Instead, we will use the FPGA to run it in simulation real-time. To do this, run `chipforge fpga --flash`. (Note: If `chipforge` is not found, run `make chipforge` to install the utility locally.) This command will take the current Caravel project, create a Vivado project based on the verilog source files, and flash it to the FPGA.

## Firmware library exploration
When using a library module written in verilog, there is usually a C firmware library written as a part of the library module. Open `library/wishbone_adder/verilog/dv/firmware`, and you will see three files, `library.makefile`, `wishbone_adder.c`, and `wishbone_adder.h`.

The `library.makefile` contains Make rules on how this library should be built. Specifically, it lists that this folder is a search path for `#include` statements and `wishbone_adder.c` should be compiled and added to the firmware executable.

The `wishbone_adder.h` file defines the public interface for the wishbone adder module. This firmware module has three main pieces:
```c
typedef struct {
  union {
    uint32_t o_sum;
    uint32_t i_a;
  };
  uint32_t i_b;
  uint32_t i_cin;
} wishbone_adder_t;
```
This is a definition of the register structure of the wishbone adder. This corresponds to the switch and assign statements in `library/wishbone_adder/verilog/rtl/wishbone_adder.v`:
```
assign wbs_dat_o = s_sum[31:0];
...
case (wbs_adr_i & 'h0000000F)
  'h0: s_wb_a <= wbs_dat_i;
  'h4: s_wb_b <= wbs_dat_i;
  'h8: s_wb_cin <= wbs_dat_i[0];
endcase
```
When writing to a wishbone_adder, the first four bytes are the `a` argument, the second four bytes are the `b` argument, and the first bit of the next four bytes is the `c` argument. The wishbone_adder computes `a + b + c` and stores it in `s_sum`. Note that there is no address checking on the `wbs_dat_o` assign statement, so reading from any of these addresses will give the sum. In this case, the struct is defined so reading from o_sum will read from the first address.

As a more concrete example, if the wishbone_adder started at address 0x4000:
```c
uint32_t add(uint32_t a, uint32_t b, bool c) {
    volatile wishbone_adder_t* adder = (volatile wishbone_adder_t*)0x4000;
    adder->i_a = a;
    adder->i_b = b;
    adder->i_c = c;
    return adder->o_sum;
}
```
This tells the compiler that there is a `wishbone_adder_t` at address 0x4000, then uses the members of the `wishbone_adder_t` structure to interact with it.

As a user of this library module, having to do all that work every time would be annoying, so there are also a few functions to perform initialization and addition:

```c
typedef struct {
  volatile wishbone_adder_t * base_addr; // Base address this wishbone adder is located at
  uint32_t wb_enable_idx;                // Index of wb_enable in the LA probes
} wishbone_adder_config_t;

// Enable and configure the wishbone adder module to use the wishbone bus
void wishbone_adder_wb_enable(const wishbone_adder_config_t config);

// Add two numbers using the wishbone adder
uint32_t wishbone_adder_add(const wishbone_adder_config_t config, uint32_t a, uint32_t b, uint32_t cin);
```

The first struct here defines a single instance of a wishbone_adder, specifically what address it starts at and where its enable pin was mapped to. Depending on how you instantiated the module in Verilog, these values will be different. 

In the Verilog `user_project_wrapper.v` above,

```verilog
`address_map(0, 32'h30123400, 32'hFFFFFF00);
`address_map(1, 32'h30123500, 32'hFFFFFF00);
```
indicate that the module connected to `wbs_datN_o[0]` starts at address `0x30123400`, and `wbs_datN_o[1]` starts at address `0x30123500`. Because `adder_0` is connected to `wbs_datN_o[0]`, we know that `adder_0` starts at address `0x30123400` and that `adder_1` starts at address `0x30123500`.

The C test in `library/wishbone_adder/verilog/dv/wishbone_adder/wishbone_adder.c` shows an example of how to use the library (side note, `library/wishbone_adder/verilog/dv/wishbone_adder/user_project_wrapper.v` is the example wishbone_adder code you copy-pasted earlier). Note that the wishbone_adder_config_t's at the start use the addresses we found above. The `wb_enable_idx` field also came from the verilog above, see if you can find why those numbers are correct.

### Summary
As a summary of how library modules are laid out:
```
wishbone_adder
  verilog
    dv
      firwmare               - Firmware library
        library.makefile       - Make configuration for this library
        wishbone_adder.c       - Firmware library implementation
        wishbone_adder.h       - Public interface
      wishbone_adder         - Example implementation + unit tests
        Makefile               - Standard Makefile for a dv test folder
        user_project_wrapper.v - Example user_project_wrapper.v instantiating the library module
        wishbone_adder_tb.v    - Verilog testbench, uses user_project_wrapper.v for simulation
        wishbone_adder.c       - Example usage of firmware library, is also unit tests for the library module
    includes
      includes.rtl.caravel_user_project - Verilog files to include in the Caravel build
    rtl
      wishbone_adder.v         - Verilog source files for the library module
```

## Using the module
You already completed the first step to using a library module, copying the example user_project_wrapper.v and running `./library/libman use ...`. Now, copy your `blink` dv test from before.

You will have to add a line to the new Makefile
```Makefile
include $(DESIGNS)/library/wishbone_adder/verilog/dv/firmware/library.makefile
```
to include the firmware library in the dv test.

Now, you can use
```c
#include <wishbone_adder.h>
```
to use the wishbone_adder firmware library from your dv test.

Final challenge: Using what you've learned previously, and the example code given as part of the library module, write a C program that uses the two wishbone_adder modules. `adder_0` should add `3 + 4 + 0`, and that result should be read from adder_0 and passed to adder_1 to compute `(3 + 4 + 0) + 5 + 1`. Ensure the results match your expectations.
