# Caravel Design

Caravel Repo Structure: https://caravel-harness.readthedocs.io/en/latest/getting-started.html#required-directory-structure

## RTL Designs
A functional RTL design can be written in Verilog and placed under the /verilog/ folder in the
caravel repository. Inside of this folder, the following actions should be made to set up a new RTL
design:

1. /verilog/rtl
    - Copy existing directory and rename files for new design
    - Edit source Verilog or create new files to reference modules

NOTE: For each of the verilog files under /verilog/includes, there MUST be an empty line after the
last entry, or the make files for the caravel repository will break. 