# Hardening

## Setting Up Sources

The tools are not installed globally on the comparch computer in Durham 310. To access the tools, run `/local/toolchain/activate` in a ssh terminal. 

NOTE: Once work is completed, you can exit the toolchain by calling `exit`

The following variables are required to run any command in the caravel repository. They reference the file path for the openlane and PDK root directories, alongside which PDK (sky130A) will be used/built. 

Run the following in the root of the caravel repository: 
```bash
export OPENLANE_ROOT=$(pwd)/dependencies/openlane_src ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Make Hardening

Functional modules are hardened under the `/openlane/` folder in the Caravel repository. 

The supplied top level design user_project_wrapper comes with a set of requirements to pass the 
eFabless precheck and fabrication and should not be edited. The only Verilog files to add to the
top-level module are the top-level functional Verilog files or hardened macros, as referenced below. 

Hardening is done by calling the make command in the root of the caravel directory: `make <module>`

## KLayout

KLayout can also be used in a Linux Coover VDI instance with an unzipped toolchain download. Refer to section `Verifying Waveforms` above to setup VDI and toolchain. 

A hardened design can be viewed in KLayout with the generated GDS file:
1. Activate Toolchain if not done so already: `~/toolchain/activate`
2. Start KLayout: `klayout`
3. In KLayout, select `File`, then `Open`
4. Navigate to the Z drive and path below to view the generated GDSII file: `...\caravel_user_project\openlane\MODULE\runs\DATE\results\signoff`

NOTE: The `\runs` directory contains an individual run for each time the `make <module>` command is called. This can take up lots of space, and will leave artifacts from incomplete runs. 

NOTE: The make command uses the folder name under `\openlane\`

## Setup KLayout Tech File

When viewing GDS files in KLayout, you can load the Skywater PDK tech file for easier viewing of different layers:
1. Open KLayout
2. Open an existing GDSII layout design
3. Select `File`, then `Load Layer Properties`
4. Select the SKY130 PDK Tech layer with the following path `Z:\caravel_user_project\dependencies\pdks\sky130A\libs.tech\klayout\tech\sky130A.lyp`

NOTE: This will need to be reloaded every time, but can be accessed again under `File`, then `Recent Sessions`.

## Generated Outputs

After running `make <module>`, the following outputs will be generated on a successful run: 
- OpenRoad Hardening reports
    - `\openlane\MODULE\runs`
- Synthesized Verilog Netlist with SkyWater PDK
    - `verilog\gl\MODULE.v`
- Place and Route Outputs
    - `lef`
    - `gds`
    - `lib`
    - `mag`
    - `maglef`
- Timing, Signoff
    - `sdc`
    - `sdf`
    - `signoff`
    - `spef`

## New Hardening Config
To setup a new module for hardening, follow the steps below:
1. /openlane/
    - Copy an existing sample hardening configuration under `/openlane/` that is NOT `user_proj_wrapper`
    - Rename the copied config folder to the same `MODULE` name as your rtl and dv design
2. /openlane/MODULE
    - Edit `config.json` with desired parameters below
    - Delete `pin_order.cfg` if not specifying north/east/south/west side for pin placement
    - Delete `macro.cfg` if not placing pre-hardened macro into hardened design

OpenROAD config parameters: https://github.com/The-OpenROAD-Project/OpenLane/blob/master/docs/source/reference/configuration.md

## Setting Pin Order
Pin order can be edited in a file `pin_order.cfg` in the desired hardening target `/openlane/MODULE/`. This is beneficial to do since you can define which side an I/O port will be connected to for a hardened macro. 

The config file should enter `#DIRECTION`, with each following newline being a top level port for the target hardened module. The cardinal direction inputs are as follows:
- `#N`: North
- `#E`: East
- `#S`: South
- `#W`: West

A wildcard `*` can be used to autofill signal names. This is useful to put all signals with certain prefixes relating to the wishbone bus, logic analyzer, or GPIO on one side. 

Pins placed on South of hardened module:
```
#S
wb_.*
io_in\[4\]
io_oeb\[1\]
```

Example: https://github.com/efabless/caravel_user_project/blob/main/openlane/user_project_wrapper/pin_order.cfg

## Setting Macro Placement
Macro placement can be edited in a file `macro.cfg` in the desired hardening target `/openlane/MODULE/`.

Each line includes 4 parameters seperated by whitespace:
1. Name of the macro (MODULE)
2. Horizontal placement of macro (unit um)
3. Verical placement of macro (unit um)
4. Orientation (N means top of module points north, no rotation)

If the orientation of the module is set to either E, W, or S, then the module will be rotated by 90 degrees per orientation change. 

Example:
```
hardened_macro 60 15 N
MODULE x y N
```

Example: https://github.com/efabless/caravel_user_project/blob/main/openlane/user_project_wrapper/macro.cfg

## Modified Config Parameters

### Submodule Designs
The following parameters inside config.json SHOULD be changed, IF NOT user_project_wrapper:
1. `DESIGN_NAME`: name of functional verilog module
2. `VERILOG_FILES`: list of related Verilog files, including submodules instantiated in hardened design
3. `CLOCK_PORT`: Clock input to module for static timing analysis
4. `CLOCK_NET`: Clock Net to module for static timing analysis
5. `FP_SIZING`: Determines how the length and width of module is determined
    - Set to `Absolute` for a fixed sized based on DIE_AREA
    - Set to `Relative` for optimized size. NOTE: Does not work for designs small enough if you have too many I/O ports
    - Default is `Relative`
6. `DIE_AREA`: Determines length and width of hardened module if `FP_SIZING` set to `Absolute`
    - Set to `x0 y0 x1 y1`, or `0 0 1000 1000` for area corners
    - Unit is μm
7. `PL_TARGET_DENSITY`: percentage from 0 to 1 of how dense cells are
    - Most designs harden up to around `0.6`
    - 1 = very dense, 0 = wide spread


### User_Project_Wrapper Config

Most config parameters should not be altered in the top level `user_project_wrapper` config file. 

Of the above parameters, the following can be updated:
1. `VERILOG_FILES`: List all used verilog files that ARE NOT pre-hardened
2. `CLOCK_PORT` and `CLOCK_NET`: Change clock port/net for timing analysis during hardening
3. `PL_TARGET_DENSITY`: Edit density of top level wrapper

Alongside using synthesized gate-level verilog netlists, OpenROAD can take pre-hardened macros and files and "drop" them into the design as pre-generated. 

To use a pre-hardened macro, edit the following in the parent config file:
1. `VERILOG_FILES_BLACKBOX`: List each pre-hardened verilog design
2. `EXTRA_LEFS`: List each pre-hardened LEF file `/lef/`
3. `EXTRA_LIBS`: List each pre-hardened GDS file under `/lib/`
4. `EXTRA_GDS_FILES`: List each pre-hardened GDS file under `/gds/`
5. `MACRO_PLACEMENT_CFG`: Set to `dir::macro.cfg` for macro placement
6. `SET_PDN_MACRO_HOOKS`: Exposes hooks for power pins. Add instantiated module name and vdd/vss ports like `user_project_example mprj`

NOTE: If you are using pre-hardened macros, you MUST specify macro placement in a macro.cfg
config file under the folder for your openlane hardening config.


## Warnings

Small macroblocks do not fit the power grid, therefore you need to avoid making small macroblocks.
For this, set the FP_SIZING to absolute and configure DIE_AREA to be bigger than 200um x 200um for sky130.
https://openlane.readthedocs.io/en/latest/tutorials/digital_guide.html#create-the-rtl-files

