# Pre-Layout Simulations

## Setting Up Sources

The tools are not installed globally on the comparch computer in Durham 310. To access the tools, run `/local/toolchain/activate` in a ssh terminal. 

NOTE: Once work is completed, you can exit the toolchain by calling `exit`

The following variables are required to run any command in the caravel repository. They reference the file path for the openlane and PDK root directories, alongside which PDK (sky130A) will be used/built. 

Run the following in the root of the caravel repository: 
```bash
export OPENLANE_ROOT=$(pwd)/dependencies/openlane_src ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Including Verilog design files

Ensure that EVERY module being tested is included in the files below. This is a list referenced when the RTL/GL/SDF simulations are run:
1. /verilog/includes/includes.gl.caravel_user_project
    - Add `-v $(USER_PROJECT_VERILOG)/gl/design/MODULE.v`
2. /verilog/includes/includes.gl+sdf.caravel_user_project
    - Add `$USER_PROJECT_VERILOG/gl/design/MODULE.v`
3. /verilog/includes/includes.rtl.caravel_user_project
    - Add `-v $(USER_PROJECT_VERILOG)/rtl/design/MODULE.v`

## Creating Testbench

Testbenches can also be used to evaluate functional RTL or a synthesized gate level netlist on its own, by directly instantiating the RTL source module, INSTEAD of the caravel module. Note that this means you are unable to test the design with C code, but this is beneficial to guarantee expected outputs before inserting the design in the overall caravel framework. 

Testbenches which are evaluating the entire caravel framework must instantiate the caravel module, which encompasses the caravel_user_wrapper top level design which digital designs are inserted into. 

To setup a Verilog testbench in the caravel repository, follow the steps below:
1. /verilog/dv/Makefile
    - Add `MODULE` name to `PATTERNS` list in makefile
2. /verilog/dv
    - Copy an existing testcase folder
    - Rename the new folder to your name, `MODULE`
3. /verilog/dv/MODULE
    - Rename the copied testbench verilog file and c file to `MODULE`
    - Delete all C code from `Module.c` except for `void main(){}`, which will be empty
4. /verilog/dv/MODULE/MODULE_tb.v
    - Set name for .vcd file by editing `$dumpfile(“MODULE.vcd”)`;
    - Set name for dumpvars by editing `$dumpvars(0, MODULE_tb)`
    - Add cmd displays with `$display(“STRING”)` in the testbench if desired

## Register Transfer Level (RTL) Simulations

RTL simulations will use behavioral verilog and C code (if desired) to run a functional simulation based on a Verilog testbench. The intent of this is to verify functionality before hardening, where the target design would be synthesized with a netlist of logic cells from the SkyWater 130nm PDK. 

To run a RTL simulation from root: `make verify-<module>-rtl`

After a simulation is run, the generated waveform from the `.vcd` file can be verified with a waveform viewer, such as `gtkwave`. 

## Verifying Waveforms

GTKWave is an open-source waveform viewer which can be accessed on the Linux Coover VDI instance and utilized with the toolchain. 

[Toolchain VDI Install](../intro/toolchain.md)

Once the VDI is installed, open the `Engineering Linux` instance. Use the following steps to install gtkwave and open it:
1. Activate toolchain with `~/toolchain/activate`
2. Open GTKWave in toolchain: `gtkwave`

To open the waveforms generated, select `File`, then `Open New Tab` in GTKWave. 

The path will be under `...\caravel_user_project\verilog\dv\MODULE\RTL-MODULE.vcd`.

## C Simulations

C Code should be added when your design has passed all other verification tests and you wish to interface with the software management core of the ASIC design. This C code can initialize the external GPIO pins, and drive inputs/outputs with the Logic Analyzer (LA) pins which are connected between the user design area and the management core. 

NOTE: C simulations take an *extremely* long time to complete, and should only be done after functionality of all Verilog designs are verified independently first. This should be one of the last simulations you run. 

C Code can be simulated in all simulations, including RTL, GL, and SDF. 