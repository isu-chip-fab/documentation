## Running Precheck

The Caravel Precheck will run multiple different tests such as DRC and LVS to ensure the design will pass fabrication. 

All Make commands are run at the root of the caravel repository.

To clone the MPW Precheck repository: `make precheck`

To run the MPW Precheck: `make run-precheck`

NOTE: This is run locally on your development machine, please ensure that the toolchain is activated. 

## Running on Remote eFabless Servers

This information was last updated on 11/23/2024. This was apart of ChipForge's first tapeout. 

Precheck and one final check (Tapeout) are required to be run with a submitted design on eFabless's remote servers before design submission. 

### Creating a Git Repo

First, start by making an account here: https://platform.efabless.com/dashboard

Then, navigate to this page to create a git repository for submission: https://repositories.efabless.com/new

NOTE: It is requried to setup an SSH key to clone down and push back up for the newly generated repository. 

NOTE: any reference to a git subrepo must be removed, or the scripts will run indefinitely. 

NOTE: Be sure not to copy any large untracked files from the existing repo. This will take a very long time to commit!

The only files the tapeout check is interested in using is the GDSII final top level file (user_project_wrapper) and the lvs config under path `lvs/user_project_wrapper/lvs_config.json`. It was reccomended to us by eFabless representatives that we still include our other design files for easier future debugging if any problems come up during the submission checks, fabrication, or packaging. 

### Running Checkers

Navigate to the following web page: https://repositories.efabless.com/dashboard/projects

Next, select `Shuttles` from the top row header, and select the target ChipIgnite project for tapeout. 

Navigate to `Manage My Submissions`, then select `Add another project to Shuttle`, and select the added git repository. 

The Tapeout check cannot be completed until the remote MPW Precheck passes. Please start by submitting the precheck, then start the tapeout check.

NOTE: Both checks could take a long time to run, on the scale of 3-10 hours. Grab your popcorn kids