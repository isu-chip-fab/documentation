# Caravel Adder

## Design Background

A ripple carry adder is a module that takes two operands `i_a` and `i_b` that are N bits wide, and sums them together with a carry in bit `i_cin`. An N bit wide output `o_sum` will output the sum between the two operands. The final carry out bit represented as bit N of the adder operation, is output as `o_cout`. Typically, ripple carry adders are implemented by daisy chaining together 1 bit Full Adders, in which the carry bit from the previous adder propogates to the next full adder. By using the `+` operand in Verilog, the synthesis tools that create an ASIC design can infer what should be placed, so we do not need to manually define each Full Adder. A Full Adder and Ripple Carry adder design is combinational, meaning that the circuit does not contain any synchonous designs such as D Flip Flops, like the previous counter design. This means that whenever any input value changes, the output value will change immediately, since it does not have to wait for the edge of a clock to write input values into DFFs. 

Explanation: https://nandland.com/ripple-carry-adder/

**Input ports:**
- `[N-1:0] i_a`: Sum operand 1
- `[N-1:0] i_b`: Sum operand 2
- `i_cin`: Carry in bit

**Output ports:**
- `[N-1:0] o_sum`: Output sum of i_a and i_b
- `o_cout`: Carry out bit

**Parameters:**
- `N`: Number of bits to add together and output (Default 32)

## VDI Connect

All work can be done with a GUI interface on the Iowa State VDI. Additionally, a command line interface can be used for faster execution time on the remote comparch lab machine. 

Please log in to the `Engineering Linux` version of the VDI, can continue work there. 

NOTE: Remember to install the [Toolchain Locally](/intro/toolchain.md?id=running-on-vdi-analog-design-or-physical-machine) to use all opensource tools on the VDI. 

To call the toolchain on the VDI to run the rest of the tutorial, call `~/toolchain/activate`. 

Additionally, the same enviornment variables from above will need to be sourced every time a terminal or toolchain is reopened. 

## Clone Repository

Run the following instructions to clone and build the public caravel repository:
1. Open a command prompt or shell on local machine
2. Run `ssh comparch`
    - The ISU VPN will be required if running off campus
    - The host `comparch` is dependent on the setup of the ssh config file
3. Clone the caravel repo to the desired directory on your Linux machine `git clone https://git.ece.iastate.edu/isu-chip-fab/caravel_user_project --recursive`
4. Navigate to the newly cloned repository `cd caravel_user_project`
5. Verify repository populated with `ls`

NOTE: If the tutorial repo was cloned in a previous tutorial, it **DOES NOT** need to be completed again. 

## Setting Up Sources

The tools are not installed globally on the comparch computer in Durham 310. To access the tools, run `/local/toolchain/activate` in a ssh terminal. 

NOTE: Once work is completed, you can exit the toolchain by calling `exit`

The following variables are required to run any command in the caravel repository. They reference the file path for the openlane and PDK root directories, alongside which PDK (sky130A) will be used/built. 

NOTE: These must be sourced in the **root** of the downloaded caravel directory. 

Run the following in the root of the caravel repository: 
```bash
export OPENLANE_ROOT=$(pwd)/dependencies/openlane_src ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Project Build

The root level Makefile includes build scripts to install each required component for simulation, hardening, and submission precheck. 

Build the projects with the command `make setup`

NOTE: If the project was built in a previous tutorial, it **DOES NOT** need to be completed again. 

## Library Setup

The library is used as before, but instead we will be using the modules `wishbone_adder` and `wishbone_helper` within `/library`. 

To add the required source files for RTL simulations, run the following libman command: `./library/libman use wishbone_adder` 

## RTL Simulations

To run sample RTL simulation provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run sample RTL simulation with `make verify-wishbone_adder-rtl@library/wishbone_adder`
3. Verify the output script reports `Test success`

NOTE: Make sure the sources above are set first, or else the simulation will not run successfully. 

## RTL Waveform Verfication

All simulations will be viewed using GTKWave in a VDI instance to access a GUI. 

Use the following steps to run gtkwave:
1. Open a terminal instance
2. Navigate to home directory: `cd`
4. Activate toolchain with `~/toolchain/activate`
5. Open GTKWave in toolchain: `gtkwave`

The waveform dump file will be under `.../library/wishbone_adder/verilog/dv/wishbone_adder/RTL-wishbone_adder.vcd`.

The path to both adder designs is listed under `wishbone_adder_tb/uut/chip_core/mprj/adder_0`. These adders contain two 32-bit inputs `i_a` and `i_b`, and a 32-bit output `o_sum`. The inputs `i_a` and `i_b` are sent over the logic analyzer pins, which connect directly between the digital design area and software management core, totalling 128 possible pins. 

There are additional ports to interact over the wishbone interface, which can send data between the digital design area and software management core within the ASIC. This is output from `o_sum` if the wishbone bus is enabled, otherwise it will default to the previous logic analyzer input signals. 

## Hardening

To run hardening provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run make command for hardening with `make user_project_wrapper@library/wishbone_adder`
3. Verify after last step that terminal outputs `[SUCCESS]: FLOW COMPLETE`

Hardened designs can be viewed as normal in Klayout like in Tutorial 1, if desired. 

## GL Simulations

NOTE: Currently, the GL simulations **DO NOT** run since the RTL simulation uses a named internal signal within the caravel core, which cannot be read with a synthesized netlist. This leads the `wishbone_adder_tb` sim to crash. For now, skip this section. 

Delete Lines 17, 18, and 20 in the following file: `/verilog/includes/includes.gl.caravel_user_project`. This will ensure the synthesized Gate Level netlist generated on the path `/library/counter/verilog/gl/` will be included in the testbench. 

Run GL Simulation: `make verify-wishbone_adder-gl@library/wishbone_adder`

GL Simulations are run with the same command, in the format `verify-MODULE-gl`, for the target MODULE to simulate. 

Run GL Simulation: `make verify-wishbone_adder-gl@library/wishbone_adder`

Verify simulation outputs the following after running the above command: `Test success`

If everything works as expected, the syntheszied netlist generated with the SkyWater PDK from OpenROAD should lead to the same exact waveform results as the RTL simulations. Verify that the outputs match the above RTL simulations to ensure that the ideal simulation for the PDK synthesis succeeded. 

## Precheck

Local precheck is not currently supported within the library submodule, since the generated output files must be at the root level directories instead of within the `/library` subdirectory. Please skip for now.

## Where to next

Next, follow through the next tutorial [03-openram](/digital/tutorial/03-openram.md). This will guide you through the same simulation, hardening and precheck steps for a design that integrates a pre-hardened memory module that cna be placed in the digital user area design. 