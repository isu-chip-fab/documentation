# Verilog Testbench Design

## Verilog Testbench Automation

Multiple different Verilog code constructs can be used to automate the flow of your testbenches in Verilog. 

Reference: https://git.ece.iastate.edu/isu-chip-fab/sddec23-06/-/blob/master/verilog/dv/user_proj_final/user_proj_final_tb.v?ref_type=heads

## Tasks

Tasks are essential to modulate your code, and act similarly to a function in C. This is useful if you are trying to replicate multiple test conditions, since you can drive varying inputs in to each task, which can drive the respective inputs and error check the outputs as designed. 

Defining a testbench task in Verilog: 
```verilog
task TASK_NAME(input TYPE INPUT_1, input TYPE INPUT_2);
    begin
        ... 
    end
endtask
```

Calling a Task: 
```verilog
TASK_NAME(INPUT_1, INPUT_2);
```

## Error Checking

Error checking your actual outputs versus your expected outputs is essential for writing strong testbenches. By creating an error signal, you can compare your outputs for each test case and evaluate if any test failed at the end of your code. An example follows below. 

Error Evaluation: 
```verilog
if(ACTUAL_OUTPUT != EXPECTED_OUTPUT)
begin
    s_error = 1'b1; //Latch error to 1 for rest of test
end
```

Error output at end of test:
```verilog
    if(s_error == 1'b0) begin //Pass indicator
        $display("Monitor: Test Passed");
        $finish;
    end

    else begin //Fail indicator
        $display("Monitor: Test Failed");
        $finish;
    end
```
