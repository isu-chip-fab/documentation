# Verilog Overview

Verilog is a Hardware Description Language (HDL) which can be used to define the functionality of digital circuits. Verilog can be synthesized and interpretted into a set of logic gates which can be used in fabrication to create an ASIC. 

## Resources

- Keywords and Sample Modules: https://nandland.com/learn-verilog/
- Interactive Exercises: https://hdlbits.01xz.net/wiki/Main_Page
- Logic Gate Modules: https://www.torporip.com/#/

## AND Gate

The sample below represents a 2 input AND gate module Verilog design. If both inputs `i_A` and `i_B` are binary 1, then the output `o_X` will also be 1. Otherwise, the output will be 0. 

The name of the module is `And_2`, and defines the port list inside the parenthesis following the module name. 

Inputs are denoted with `input`, and outputs are denoted with `output`. Inputs to the module will be fed in from either another module design or a testbench input, and the output can be read by other modules by connecting a signal to the output. 

The output is driven with the `assign o_X` statement. The `and` syntax in this assign statement is a dataflow model of how an AND gate will function. 

The design is wrapped starting with `module` and complete with `endmodule`. This `And_2` design can be instantiated in another design or a testbench to be utilized and tested. 

```verilog
module And_2
  (input  i_A,
   input  i_B,
   output o_X);
   
  assign o_X = i_A and i_B;
   
endmodule
```

## N-Bit 2x1 Multiplexer

A 2x1 Multiplexer will select 1 of 2 potential inputs to be routed to the output. There will be log2(N) select bits for every N inputs, to select the output. 

Ports and signals in Verilog can be more than 1 bit, or defined as a bus of bits. This is showcased by defining inputs and outputs with multiple bits such as `input [g_WIDTH-1:0] i_Data1`, which means the input port `i_Data1` will be `g_WIDTH` bits wide. 

`g_Width` is defined by the parameter after the module name with `#(parameter g_WIDTH = 32)`. The `#` represents that this is a paramter list, which can be used to define default sizes and scopes of the design. It is commonly used to define bit widths, and will be hardset upon instantiation of the design so that it CANNOT be changed. This is beneficial since the bit size can be altered if a 2x1 MUX was needed for 8-bit wide signals instead of 32-bit.

A ternary operator `?` is used to model the behavior of the MUX. This operator is formatted as `condition ? if true : if false`. In this example, if `i_Select` is 1, then `i_Data1` will be routed to the output `o_Data`. Otherwise, `o_Data` will be assigned to `i_Data2`. 

```verilog
module Mux_2_To_1_Width #(parameter g_WIDTH = 32)
  (input  i_Select,
   input  [g_WIDTH-1:0] i_Data1,
   input  [g_WIDTH-1:0] i_Data2,
   output [g_WIDTH-1:0] o_Data);
   
  assign o_Data = i_Select ? i_Data1 : i_Data2;
   
endmodule // Mux_2_To_1_Width
```

## 1-bit Full Adder

A full adder is used to sum together two bits and a carry in value. Each input and output is 1-bit wide. The inputs to be summed include `a`, `b`, and `cin`. The sum outputs are `sum` and `carry`, in which `sum` is the least significant sum output for bit 0, and `carry` is the bit 1 sum output. Addition is carried out in binary, and the `carry` output can be thought of as adding +1 to next base of values to be added.

The first modeling of the full adder is done structurally, which utilizes logic gates such as the previously explained `And_2` module to define the circuit. This can be derived with a truth table and boolean algebra, and is the most direct way to define a design. The tradeoff is this typically takes many more lines of code to define. 

```verilog
//Full adder using structural modeling
module full_adder_s (
    input a,b,cin,
    output sum,carry
);

wire w1,w2,w3,w4;       //Internal connections

xor(w1,a,b);
xor(sum,w1,cin);        //Sum output

And_2(w2,a,b);
And_2(w3,b,cin);
And_2(w4,cin,a);

or(carry,w2,w3,w4);     //carry output

endmodule
```

The second option to modelling Verilog, and the full adder, is to design a data-flow model. This implementation will use Verilog operators to mimick the behavior of the structural logic gates instead. This means that instead of instantiating each basic logic gate, the behavior can be defined instead to reduce the derived outputs for `sum` and `carry` into one line boolean algebra expressions. 

```verilog
//full adder using data-flow modeling
module full_adder_d (
    input a,b,cin,
    output sum,carry
);

assign sum = a ^ b ^ cin;
assign carry = (a & b) | (b & cin)  | (cin & a) ;

endmodule
```

Another way to model the full adder is by using the `+` operator and bit-wise concatenation. 

Signals can be grouped into bundles by presenting a set of signals or constant values between `{Most significant Bit, ..., Least Significant Bit}`. Since the adder output is multiple bit wides, due to a carry bit, the outputs `sum` and `carry` are concatenated so that they are assigned the proper bits. The leftmost signal in bitwise concataneation will correspond to the most significant bits to the assign statement. Each signal following will be assigned to lower and lower indices for the signals. 

```verilog
//full adder using Verilog Operator
module full_adder_o (
    input a,b,cin,
    output sum,carry
);

assign {carry,sum} = a+b+cin;

endmodule
```

## D Flip Flop

A D Flip Flop is used for synchronous digital designs, and can be used to retain the memory of a value. 

In Verilog, an `always` block is used for synchronous designs. The `@(posedge i_clk, posedge i_rst) ` structure represents the senesitivity list, which means the code inside the always block will ONLY be evaluated when the signals inside the paranthesis change. If the signals are defined as either `posedge` or `negedge`, they will only trigger when changing from 0 to 1 or 1 to 0 respectively. If an `always` block is more than one line of code, it will need to include a `begin` and `end` for the block of code to be run. 

Synchronous designs include a clock, represented with input `i_clk`, such that when the clock changes from logic level 0 to 1, then stored bit will be reevaluated, due to the `posedge` in the sensitivity list. In a similar fashion, if the reset signal `i_rst` is asserted from 0 to 1, the code will also be reevaluated. 

Inside of an `always` block, `if else` statements can be used **carefully**. In this case, if the input reset `i_rst` is set from 1 to 0, the stored value `o_Q` will be set to logic level 0. This is beneficial in a design since it can help restore to a known state, where all stored data defaults to something expected. Otherwise, the output `o_Q` will be set to the data input `i_D`, which will write a new stored value to the D Flip Flop. Note that this is synchronous to the clock input `i_clk`, meaning it will only update on the positive edge of the clock. 

The output `o_Q` is defined as a `reg` type in the port list, which means it will retain a stored value after being set, unlike the default `wire` type that has been used up to this point. In the case of an `always` block and synchronous design, it is good practice to use a blocking `<=` assignment when writing values to synchronous `reg` signals. In the case of wires, non-blocking `=` assignments should be used when assigning values to combinational circuits.

```verilog
//full adder using Verilog Operator
module DFF(
    input i_clk,
    input i_rst,
    input i_D, 
    output reg o_Q
);

always @(posedge i_clk, negedge i_rst) 
begin
    if(i_rst==1'b1)
        o_Q <= 1'b0; 
    else
        o_Q <= i_D; 
end 
endmodule 
```