# Digital Tutorial Index

Tutorial Walkthroughs:
1. [01 - Counter](01-counter.md)
2. [02 - Adder](02-adder.md)
3. [03 - OpenRAM](03-openram.md)

Digital Design Introduction:
- [Verilog](verilog.md)
- [Verilog Testbench](testbench.md)
