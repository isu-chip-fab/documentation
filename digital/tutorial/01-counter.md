# Caravel Counter

## Pre-requisites

This assumes you already have [ran through the full introduction](/intro/intro/new_members?id=off-campus-vpn) to setup the required tools. 

Specifically, you will need:
- Off-campus VPN
- VDI Installed
- SSH connection to comparch

Additionally, you will need the  [Local Toolchain Installed](/intro/toolchain.md?id=running-on-vdi-analog-design-or-physical-machine) for use on the VDI. 

## VDI Connect

All work can be done with a GUI interface on the Iowa State VDI. Additionally, a command line interface can be used for faster execution time on the remote comparch lab machine. 

Please log in to the `Engineering Linux` version of the VDI, can continue work there. 

NOTE: Remember to install the [Toolchain Locally](/intro/toolchain.md?id=running-on-vdi-analog-design-or-physical-machine) to use all opensource tools on the VDI. 

To call the toolchain on the VDI to run the rest of the tutorial, call `~/toolchain/activate`. 

Additionally, the same enviornment variables from above will need to be sourced every time a terminal or toolchain is reopened. 

## Clone Repository

Run the following instructions to clone and build the public caravel repository:
1. Open a command prompt or shell on local machine
2. Run `ssh comparch`
    - The ISU VPN will be required if running off campus
    - The host `comparch` is dependent on the setup of the ssh config file
3. Clone the caravel repo to the desired directory on your Linux machine `git clone https://git.ece.iastate.edu/isu-chip-fab/caravel_user_project --recursive`
4. Navigate to the newly cloned repository `cd caravel_user_project`
5. Verify repository populated with `ls`

NOTE: Cloning the git repository with `--recursive` enables the library submodule which contains our source design to be included. The library submodule can also be updated after cloning with th following command: `git submodule update --init`

## Setting Up Sources

The tools are not installed globally on the comparch computer in Durham 310. To access the tools, run `/local/toolchain/activate` in a ssh terminal. 

NOTE: Once work is completed, you can exit the toolchain by calling `exit`

The following variables are required to run any command in the caravel repository. They reference the file path for the openlane and PDK root directories, alongside which PDK (sky130A) will be used/built. 

NOTE: These must be sourced in the **root** of the downloaded caravel directory. 

Run the following in the root of the caravel repository: 
```bash
export OPENLANE_ROOT=$(pwd)/dependencies/openlane_src ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Project Build

The root level Makefile includes build scripts to install each required component for simulation, hardening, and submission precheck. 

Build the projects with the command `make setup`

## Design Background

The primary intent of this tutorial is to step through all of the tools involved in the digital ASIC design process. In this tutorial, a counter provided in the default design repository will be simulated, synthesized, and hardened. 

This default design implements two modes of communicationw within the larger ASIC system, including a Wishbone Bus and Logic Analyzer pins. For now, we will ignore what these do and focus on the simulated design. A counter is a synchronous design, which requires values to be stored in D Flip Flops (DFF). DFFs use a clock to determine when to write data to store within them, and a write occurs on an edge of a clock, when the clock signal switches from 0 to 1 (positive edge) or 1 to 0 (negative edge). Additionally, the stored value in a DFF can be set back to a default value with a reset signal, which can be done either at the same time as the clock edge (synchronous) or independently whenever the reset is asserted (asynchronous). 

Data can be written to the counter from the Wishbone Bus with the signals `wstrb` and `wdata`. The input port `wstrb` determines which bytes from `wdata` will be written to `count`. Each bit of `wstrb` corresponds to a different 8-bit byte in the stored `count` register. 

Data can be written from the Logic Analyzer pins with the input ports `lawrite` and `la_input`. When `lawrite` is nonzero, then data will be written from `la_input` and stored in the `count` register. Note that `la_write` is multiple bits, and onlyt the bits set to 1 in `la_write` will be written from `la_input` into `count`. 

The output `ready` indicates if the counter is reset and able to begin counting. The outptu `rdata` indicates the initial reset or stored count value. The output `count` represents the count value that is either written to or incremented throughout operation of the synchronous design. 

**Input ports:**
- `clk`: Positive edge clock
- `reset`: Active high synchronous reset
- `valid`: Is wdata valid?
- `[3:0] wstrb`: Which count bytes to write to from wishbone bus
- `[BITS-1:0] wdata`: Data to write to stored count value from wishbone bus
- `[BITS-1:0] lawrite`: Write enable to stored count value from Logic Analyzer Pins
- `[BITS-1:0] la_input`: Data to write to stored count value from Logic Analyzer pins

**Output ports:**
- `ready`: Indicates counter reset and ready to increment or be written to
- `[BITS-1:0] rdata`: Outputs initial stored count value 
- `[BITS-1:0] count`: Outputs stored count value

**Parameters:**
- `BITS`: Number of bits to count to (default 32)

## Library Setup

The library submodule is where all Chip Forge IP is intended to be stored. It is a git submodule, which will be updated with the above `--recursive` command. The `counter` Verilog design, testbenches, and hardening configs are all stored under the path `/library/counter` within the `caravel_user_project` repository. 

Add the counter module to the verilog includes for simulation: `./library/libman use counter` 

Replace Lines 17 and 18 in the following file with the contents below: `/verilog/includes/includes.rtl.caravel_user_project`
```
`-v $(DESIGNS)/library/counter/verilog/dv/counter/user_project_wrapper.v`
`-v $(DESIGNS)/library/counter/verilog/dv/counter/user_proj_example.v`
```

## RTL Simulation

Register Transfer Level (RTL) Simulations can be used to verify the functional Verilog designs that will be placed in the user area of an ASIC. This is done BEFORE logic synthesis and place and rotue (Hardening). 

To run sample RTL simulation provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run sample RTL simulation with `make verify-counter-rtl@library/counter`
3. Verify the output script reports `LA Test 2 Passed`

NOTE: The syntax `make target@library/counter` is required for any command that is targetting a simulation or hardening config within the library submodule. 

## Verifying Waveforms

GTKWave is an open-source waveform viewer which can be accessed on the Linux Coover VDI instance and utilized with the toolchain. 

Use the following to run gtkwave:
1. Activate toolchain with `~/toolchain/activate`
2. Open GTKWave in toolchain: `gtkwave`

To open the waveforms generated, select `File`, then `Open New Tab` in GTKWave. 

The path will be under `.../caravel_user_project/library/counter/verilog/dv/counter/RTL-counter.vcd`. 

The waveform can be verified by monitoring the `la_input` signal from the management core, which acts as the software processor the Caravel frame. 

On the left hand side, navigate through the module heirarchy drop downs from `counter_tb/uut/chip_core/soc`.

Once `soc` is selected, select the wire `la_input` in the window below the modules and select `Append` to add the signal to the waveform viewer window. 

You can zoom in with the plus key on the top toolbar or by holding down `ctrl` and scrolling up on a mouse. This test verifies that the counter will increment by 1, and is written back from the digital user area design to the software processor over the logic analyzer pins. 

## Hardening user_project_wrapper

`Hardening` refers to the process which is utilized with OpenROAD, which will synthesize, place and route, and run error checks on a list of Verilog design files to be placed in the user area. To generate a synthesized logic netlist of logic cells, hardening must be done! 

The `user_proj_example` module contains the counter design which was simulated in the above RTL simulation. The source code for this design is located under `library/counter/verilog/user_proj_example`.

All hardening configs are kept under `/openlane/MODULE` for a normal project, but can also be housed within the library submodules under path `/library/MODULE/openlane/`. In this tutorial the hardening configs are kept in the library submodule under the path `/library/counter/openlane/user_proj_example`. 

The `user_proj_example` module is hardened as its own macro, which will be placed in the top level design of `user_project_wrapper`. There is also another hardening config option to directly synthesize the Verilog design on the top level, which requires different hardening config settings. 

To run hardening provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run make command for hardening with `make user_proj_example@library/counter`
3. Verify after last step that terminal outputs `[SUCCESS]: FLOW COMPLETE`

## Viewing Hardened Design

KLayout can also be used in a Linux Coover VDI instance with an unzipped toolchain download. 

A hardened design can be viewed in KLayout with the generated GDS file:
1. Activate Toolchain if not done so already: `~/toolchain/activate`
2. Start KLayout: `klayout`
3. In KLayout, select `File`, then `Open`
4. Navigate to the Z drive and path below to view the generated GDSII file: `.../caravel_user_project/library/counter/openlane/user_proj_example/runs/DATE/results/signoff`

NOTE: The `/runs` directory contains an individual run for each time the `make <module>` command is called. This can take up lots of space, and will leave artifacts from incomplete runs. 

NOTE: The make command uses the folder name under `/openlane/` traditionally. It can also be specified to target an openlane folder in the library submodule under the path `/library/MODULE/openlane/`. 

## Hardening user_project_wrapper

Next the top level module `user_project_wrapper` of the user design area can be hardened, which will place the pre-hardened `user_proj_example` within it.

To run hardening provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run make command for hardening with `make user_project_wrapper@library/counter`
3. Verify after last step that terminal outputs `[SUCCESS]: FLOW COMPLETE`

## GL Simulation

A Gate Level (GL) simulation will use the synthesized netlist from the Verilog designs with the SkyWater130 PDK logic cells to rerun the same testbench simulation. The goal is to verify that the same functionality repeats from the RTL simulation, to ensure that logic synthesis did not alter any functionality of the ASIC design. 

Delete Lines 17, 18, and 20 in the following file: `/verilog/includes/includes.gl.caravel_user_project`. This will ensure the synthesized Gate Level netlist generated on the path `/library/counter/verilog/gl/` will be included in the testbench. 

To run sample GL simulation provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run sample RTL simulation with `make verify-counter-gl@library/counter`
3. Verify the output script reports `LA Test 2 Passed`

GTKWave can be used in a similar fashion to view the waveforms. Note that the GL waveform outputs will begin with the `GL-` prefix instead of `RTL-`. 

NOTE: The Gate Level simulations take longer to run than the RTL simulations. Especially with C code used for the testbenches. 

## Precheck

Local precheck is not currently supported within the library submodule, since the generated output files must be at the root level directories instead of within the `/library` subdirectory. Please skip for now.

## Where to next

Next, follow through the next tutorial [02-adder](/digital/tutorial/02-adder.md). This will guide you through the same simulation, hardening and precheck steps for a design that integrates an adder over both the logic analyzer ports and wishbone bus. 