# OpenRAM Overview

## Design Background

### OpenRAM Design

Each OpenRAM design can contain 1 or multiple read/write or read-only memory ports. Memory can be read per 8-bit byte or per 32-bit word, with varying amounts of total memory size. 

Each read/write or read-only port has the following input and output ports PER PORT. Note that the read only port does not include `wmask` and `web` input ports, since it is read only. 

**Input ports:**
- `clk`: Clock
- `csb`: Active Low Chip Select
- `web`: Active Low Write Enable
- `wmask`: Write mask, defines which byte to write to (if more than one)
- `addr`: Address 
- `din`: Data In

**Output ports:**
- `dout`: Data Out per Address

**OpenRAM Module Parameters:**
- `NUM_WMASKS`: Number of write mask bits (typically 1 or 4)
- `DATA_WIDTH`: Number of data bits read at once
- `ADDR_WIDTH`: Number of address bits
- `DELAY`: # of cycle delays to read 
- `VERBOSE`: Displays print statements for debugging. Set to 0 if not used
- `T_HOLD`: # of cycles to hold output data after posedge, arbitrary

There is a three-cycle clock delay after the address is input before the data is able to be read from memory. 

### Sourcing the memory design

There are three primary locations to obtain an OpenRAM design:
1. Use the OpenRAM tool to generate a custom design: https://github.com/VLSIDA/OpenRAM
2. Use the provided PDK provided with `make setup` in the Caravel toolflow
3. Use the designs in the second OpenRAM test chip: https://github.com/VLSIDA/openram_testchip2. 

In our case, we will be sourcing our prehardened openram macro from the same ChipForge library repo as the `wishbone_adder` design. 

### RTL Design

To ensure easiest access to all metal layers and power rails, the OpenRAM design **must** be placed in the top level `user_project_wrapper` design. If the OpenRAM memory cell is not placed inside the `user_project_wrapper` module, it will have access to one less metal layer per level deeper.

### Hardening 

The hardening config file is referenced from the second OpenRAM test chip: https://github.com/VLSIDA/openram_testchip2/blob/main/openlane/user_project_wrapper/config.json

## Running Tutorial

## VDI Connect

All work can be done with a GUI interface on the Iowa State VDI. Additionally, a command line interface can be used for faster execution time on the remote comparch lab machine. 

Please log in to the `Engineering Linux` version of the VDI, can continue work there. 

NOTE: Remember to install the [Toolchain Locally](/intro/toolchain.md?id=running-on-vdi-analog-design-or-physical-machine) to use all opensource tools on the VDI. 

To call the toolchain on the VDI to run the rest of the tutorial, call `~/toolchain/activate`. 

Additionally, the same enviornment variables from above will need to be sourced every time a terminal or toolchain is reopened. 

### Clone Repository

Run the following instructions to clone and build the public caravel repository:
1. Open a command prompt or shell on local machine
2. Run `ssh comparch`
    - The ISU VPN will be required if running off campus
    - The host `comparch` is dependent on the setup of the ssh config file
3. Clone the caravel repo to the desired directory on your Linux machine `git clone https://git.ece.iastate.edu/isu-chip-fab/caravel_user_project --recursive`
4. Navigate to the newly cloned repository `cd caravel_user_project`
5. Verify repository populated with `ls`

NOTE: If the tutorial repo was cloned in a previous tutorial, it **DOES NOT** need to be completed again. 

## Library Setup

The library is used as before, but instead we will be using the module `openram_8x1024`. 

To add the required source files for RTL simulations, run the following libman command: `./library/libman use openram_8x1024` 

## Setting Up Sources

The tools are not installed globally on the comparch computer in Durham 310. To access the tools, run `/local/toolchain/activate` in a ssh terminal. 

NOTE: Once work is completed, you can exit the toolchain by calling `exit`

The following variables are required to run any command in the caravel repository. They reference the file path for the openlane and PDK root directories, alongside which PDK (sky130A) will be used/built. 

NOTE: These must be sourced in the **root** of the downloaded caravel directory. 

Run the following in the root of the caravel repository: 
```bash
export OPENLANE_ROOT=$(pwd)/dependencies/openlane_src ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Project Build

The root level Makefile includes build scripts to install each required component for simulation, hardening, and submission precheck. 

Build the projects with the command `make setup`

NOTE: If the project was built in a previous tutorial, it **DOES NOT** need to be completed again. 

### RTL Simulations

The openram testbench **DOES NOT** use the microcontroller core, so it is not instantiated under the caravel framework. Instead, as a smaller example, the openram module is instantiated on its own, and driven by verilog testbench inputs to drive the data to be written and addreses to read from. 

To run sample RTL simulation provided by the caravel repo:
1. Enter the root of the caravel repo with `cd`
2. Run sample RTL simulation with `make verify-openram_8x1024-rtl@library/openram_8x1024`
3. Verify the output script reports `Test success`

NOTE: Make sure the sources above are set first, or else the simulation will not run successfully. 

### RTL Waveform Verfication

Use the following steps to run gtkwave:
1. Open a terminal instance
2. Navigate to home directory: `cd`
4. Activate toolchain with `~/toolchain/activate`
5. Open GTKWave in toolchain: `gtkwave`

To open the waveforms generated, select `File`, then `Open New Tab` in GTKWave. 

The path will be under `/library/openram_8x1024/verilog/dv/openram_8x1024/RTL-openram_8x1024.vcd`.

All relevent signals will exist on the top level testbench `openram_tb` since 

### Hardening

Since our design is already a prehardened macro, only the top level `user_project_wrapper` will need to be hardened: `make user_project_wrapper@library/openram_8x1024`

NOTE: Due to the new updates to the skywater PDK, OpenRAM will fail the Magic DRC step of hardening. 

## Precheck

Local precheck is not currently supported within the library submodule, since the generated output files must be at the root level directories instead of within the `/library` subdirectory. Please skip for now.
