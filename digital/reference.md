# Caravel Reference

## Other User Guides
- Digital: https://sddec23-06.sd.ece.iastate.edu/reports/492/UserGuide.pdf
- ReRAM: https://sddec23-08.sd.ece.iastate.edu/reports/Senior%20Design%20Enviornment_ReRAM%20Setup.pdf

## Getting Started
- Quickstart: https://caravel-user-project.readthedocs.io/en/latest/
- OpenMPW: https://efabless.com/kb-articles/creating-your-first-open-mpw-or-chipignite-project
- Simulation: https://caravel-user-project.readthedocs.io/en/latest/#running-full-chip-simulation

## Reference
- Harness: https://caravel-harness.readthedocs.io/en/latest/
- MGMT SoC: https://caravel-mgmt-soc-litex.readthedocs.io/en/latest/
- Wishbone Bus: https://cdn.opencores.org/downloads/wbspec_b4.pdf
- OpenRAM: https://vlsi.jp/OpenMPWSRAM_eng.html#using-sram-with-openmpw
- Bring-up: https://github.com/efabless/caravel_board/tree/main

## SkyWater PDK
- PDK DRC Rules: https://skywater-pdk.readthedocs.io/en/main/rules/periphery.html
- Layer List: https://skywater-pdk.readthedocs.io/en/main/rules/layers.html#gds-layers-information

## Terminology

- **MPW**: Multi Project Wafer, each ASIC design is placed on a wafer to be fabricated alongisde multiple other designs
- **eFabless**: Name of the company which offers the free MPW service, alongside a paid service called ChipIgnite
- **Caravel**: Name of the overall SoC core that our design goes into, houses a RISCV management core, I/O with design area, and power management

- **RTL Simulation**: Register Transfer Level Simulation, uses functional Verilog and C Code to test modules to generate waveform outputs
- **GL Simulation**: Gate Level Simulation, uses *synthesized* Verilog netlists with Skywater130 PDK and C Code to generate waveform outputs

- **Synthesis**: Part of `hardening`, utilizes Skywater130 PDK standard cells to make gate level netlist from functional Verilog design
- **Place and Route**: Part of `hardening`, places synthesized netlist with Skywater130 PDK and routes interconnects between gates to meet timing requirements
- **DRC**: Design Rule Check, Part of `hardening`, runs set of fixed rules to verify design will meet timing and area requirements 
- **LVS**: Layout Versus Schematic, Part of `hardening`, compares final layout netlist with initial synthesized netlist, used for design verification
- **Hardening**: Utilizes Verilog, Skywater PDK, and Config JSON files to synthesis, place and route, and error check modules to generate GDSII files

- **Precheck**: Acceptance testing for eFabless submission, runs LVS and DRC checks on `user_project_wrapper` and included designs
- **Bringup**: Internal name with ChipForge for post-silicon ASIC validation, see `Bringup Guide` page
