# Digital Index

Are you designing/testing Digital hardware for an ASIC?
* [Tutorial - Start Here](tutorial/index.md)

Run through the following pages in order to gain an introduction to the digital ASIC design flow:
1. [Design](design.md)
2. [Pre-Layout Simulation](pre_layout_sim.md)
3. [Hardening](hardening.md)
4. [Post-Layout Simulation](post_layout_sim.md)
5. [Submission Checks](submission.md)

To program an ASIC design to an FPGA:
- [FPGA](../intro/cf/fpga.md)

Supplemental Information:
- [Reference](reference.md)