# Post-Layout Simulations

Run the following in the root of the caravel repository: 
```bash
export OPENLANE_ROOT=$(pwd)/dependencies/openlane_src ||
export PDK_ROOT=$(pwd)/dependencies/pdks ||
export PDK=sky130A

```

## Gate Level (GL) Simulations

Gate level simulations will come **after** you have successfully synthesized your target Verilog design and generated a netlist with the SkyWater 130nm PDK. The same testbench can be utilized, and if synthesis was successful, then your waveform outputs should match your RTL simulations. 

To run a GL simulation from root: `make verify-<module>-gl`

GTKWave can be used to verify your gate level design also, which can be used with the vcd file `GL-MODULE.vcd`

## SDF Simulations

SDF Simulations are used to simulate a design after a Verilog design has been synthesized into a gate level netlist and also been through placement. This is extremely beneficial since it is the closest a design will be to a fabricated design, since it includes estimated gate delays to ensure timing requirements are met. 

NOTE: SDF Simulations will occur **after** hardening is completed for the target design module. 

To run a SDF simulation from root: `make verify-<module>-gl-sdf`

As of 4/14/2024, the SDF simulations do not run with the sample `user_proj_example` design in the Caravel framework. The following error is reported: 

```
Unresolved modules or udps:
  RAM128
  RAM256
  sky130_ef_sc_hd__decap_12
  constant_block
  mprj_io_buffer
  caravan_signal_routing
  empty_macro_1
  user_analog_project_wrapper

  8 modules or udps unresolved after library processing - cannot continue.
  Unable to begin simulation.
```

It is assumed that these modules are being pulled in from the `caravel.v` or `caravel_core.v` source files, even though they might not all be used directly for the target sample design, such as `user_analog_project_wrapper.v`. 

As of now, SDF simulations cannot be run. In the future, hopefully the main Caravel repo is updated so post-layout timing simulations can be achieved. In the meantime, Static Timing Analysis (STA) is still completed during the hardening process, and will cause precheck to fail if invalid. 


## Independent Static Timing Analysis

Static Timing Analysis is conducted as part of the hardening phase, and corresponding reports are generated. If timing is failed, it can be rerun indepedently to save time going through hardening steps that previously passed.

Run the following make commands at root in order: 
1. `make extract-parasitics`
2. `create-spef-mapping`
3. `make caravel-sta`