# Git Repository

## Hardware Repository

The hardware repo is managed within the ChipForge git repo, which is located below. 

Git Repo: https://git.ece.iastate.edu/isu-chip-fab/hardware

Once a new commit is pushed, a CI/CD runner will generate an output for the schematic and layout PDFs, a corresponding BOM, and a link to a 3D PCB viewer at the following website link.

Website Link: https://git-pages.ece.iastate.edu/isu-chip-fab/hardware/

The following contents will be generated per board:
- `README.md`: 
- `DESIGN-bom.csv`: List of all PCB components in CSV format
- `DESIGN-bom.html`: List of all PCB components in HTML format
- `DESIGN-gerbers.zip`: Zip file of all PCB layers to be submitted for fabrication
- `DESIGN-model.wrl (link)`: Link to 3D layout viwer
- `DESIGN-pcb.pdf`: Layout of PCB Layout in PDF format
- `DESIGN-sch.pdf`: Schematic in PDF format
- `DESIGN-src.zip`: Zip of entire KiCAD project
- `project_hash`: Hash of generated project

To summarize, refer to the following links for desired action. 

Design Review:
- `DESIGN-sch.pdf`: Review Schematic
- `DESIGN-pcb.pdf`: Review Layout
- `DESIGN-model.wrl (link)`: Review Layout


Parts Order:
- `DESIGN-bom.csv`: Add CSV to parts retailer order for quantity/part numbers

PCB Order:
- `DESIGN-gerbers.zip`: Submit zip file to PCB manufacturer for assembly


## KiCAD

The open-source tool KiCAD is used to design the schematics and PCB layout for each hardware board. 

KiCAD Install: https://www.kicad.org/download/