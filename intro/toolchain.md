# Toolchain

The internal toolchain supported by ChipForge is required to be used when accessing the shared computers or when using a VDI. The toolchain provides access to all required open-source tools for ASIC design and bringup. 

## Running on Comparch or Draccus

The toolchain is located under `/local/toolchain` in the local Linux Coover machine instances. If you are SSHing into a machine to use your development tools, use this route. 

To activate toolchain: `/local/toolchain/activate`

NOTE: No installation is required to run this version of the toolchain. The above command can be called by default as long as you are SSHed into the comparch machine in Durham 310. 

## Running on VDI, Analog Design, or Physical Machine

The following curl and extract commands are required to run the toolchain within a Linux VDI instance, which has the benefit of a GUI, which allows many more tools to be used. 

Extract toolchain to X Drive: 
```sh
curl -L https://git.ece.iastate.edu/api/v4/projects/6423/packages/generic/toolchain/1.0.4/toolchain.tar.gz | tar xz
```

To activate toolchain: `~/toolchain/activate`

## Exit Toolchain

When done working with ChipForge repositories, the toolchain can be exited with the command `exit`. 

NOTE: This will stop all dependencies from being referenced, and another activate call will be required again before any tools can be used. 

NOTE: the call to activate the toolchain will be required on every startup to the ssh. 

## Git Repository

Managed Git Repo: https://git.ece.iastate.edu/isu-chip-fab/toolchain