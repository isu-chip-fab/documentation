# New Members Guide

This page is inteded for new members of ChipForge, which will guide you through all required tools and accesses to begin designing or testing an open-source ASIC. Complete each step in order, and refer to the ChipForge Team's channel if there are issues. 

## Off-Campus VPN

The Cisco AnyConnect VPN can be used to connect to on-campus computers for work on bringup or ASIC design from anywhere. By following the guide below and logging in with your ISU netid, you will be able to access anything on the regular ISU network.

Installation and usage guide from ISU Engineering Technology Support: https://it.engineering.iastate.edu/how-to/install-and-connect-to-vpn-pc/

## Durham 310 Access
**Optional if you will be working on boards in-person**
All ASIC resources and workstations are housed in the lab at Durham 310 on campus. 

Request access for keys here: https://keys.ece.iastate.edu/

NOTE: the above link can only be accessed on the ISU network or by VPN (See Above).

Request access for the following rooms:
- Durham du310 (lab access)
- Durham Durham Exterior (access to building)

Set the approver as `Henry Duwe` so it can be approved ASAP. 

Both doors can be used with your ISU student ID. Only the left door to Durham 310 is accessible by key card. 

## Connecting to the webcam (optional)

If you do not have physical access to a board or don't want to be in person, we have a webcam set up in Durham 310 you can access to view the physical boards.

To connect to the webcam, open a browser to [http://xilinxcam-6a.ece.iastate.edu](http://xilinxcam-6a.ece.iastate.edu) and login using `chipfab` and password `chipfab`.


## Installing VDI

A VDI instance can be used to remote into a Linux instance of the ECPE computers. When combined with the toolchain, this allows for a GUI interface and standardized tools for Analog design tools, waveform viewers, and GDSII viewers. 

Install the VDI per ETG instructions here: https://etg.ece.iastate.edu/vdi/

The target VDI instance we will use is `Engineering Linux` for all work. 

## Connecting to Comparch Machine
The normal syntax for an ssh command is `ssh user@host`. When you have a long command, it is often easier to add an entry to your ssh config file.

Create a new file in the `.ssh` folder in your local machine named `config`, or edit that file if it exists already.

The ssh config entry for the comparch machine looks like:
```
Host comparch
    HostName comparch-dev02.ece.iastate.edu
    User <netid>
```

NOTE: The User `<netid>` should be changed to your unique Iowa State netid. 

**Windows:**
1. Run `notepad C:\Users\username\.ssh\config` in command prompt to create SSH config file under `\.ssh\` folder. Replace `username` with your local username.
    - If the file already exists, this will open it
2. Copy above contents, and change `netid` to your ISU netid (eg. jmhafele)
3. Save the config file and close the window
4. ***IMPORTANT*** This file _must_ be saved with no file extension. If it is saved as `config.txt`, it will _not_ work. Use the command above to save it with no file extension, or remove the file extension after creating the file. It is not enough to simply hide the file extension in File Explorer.

**MacOS/Linux:**
1. Open `~/.ssh/config` with desired text editor
2. Copy above contents, and change `netid` to your ISU netid (eg. jmhafele)
3. Save the config file and close the window

And now instead of running `ssh netid@comparch-dev02.ece.iastate.edu`, you can just run `ssh comparch`. This allows you to open a remote terminal on the comparch machine. In the future, when running a command _over ssh_ is mentioned, that means to run `ssh comparch`, then run the command in the remote terminal.

VSCode will also read the SSH config file and automatically show it in the list of SSH hosts to connect to later.

## Authentication

Git is used through a command-line utility git in a terminal. The most secure and reliable method for allowing git to access your GitLab account is through an SSH Key. SSH Keys are effectively long randomized passwords which are generated uniquely for each device you use. You will need to follow these instructions on each device you wish to have access to your GitLab account from the git utility. Each device you use will have its own SSH key to log in, and you can revoke keys at any time from the GitLab Account Settings page.

SSH keys work as a pair of public and private key. The public key is shared with anyone who needs to be able to confirm your identity (e.g. GitLab), wheras the private key is the actual identity itself. By default, the keys will be generated in a hidden folder .ssh in your home directory named id_ed25519 for the private key and id_ed25519.pub for the public key.

**The private key must be kept private and never shared with anyone or they will be able to log in as you.**

### Create an SSH Key

An SSH Key will need to be generated both on your local machine to access Comparch and to use git on the Comparch machine. An SSH key will act as a password, and remove the need for inputting a password for every machine login or push to git. 

If you already have an SSH key you would like to use with GitLab, you may skip this step and proceed to adding the key to GitLab.

**Windows:**
1. Open a Command Prompt.
2. Run `ssh-keygen -t ed25519` to generate a new SSH key. Continue pressing enter until the program exits to accept the defaults. If you already had an SSH key on your computer, it will not be overwritten.
3. Run `notepad.exe C:\Users\username\.ssh\id_ed25519.pub` to open your public key in notepad. Copy the entire contents of this file to paste later.
    - username will need to be updated on the above path to your Windows username

NOTE: The path for the .ssh folder on windows is `C:\Users\user_id\.ssh`

**MacOS/Linux:**
1. Open a Terminal.
2. Run `ssh-keygen -t ed25519` to generate a new SSH key. Continue pressing enter until the program exits to accept the defaults. If you already had an SSH key on your computer, it will not be overwritten.
3. Run `cat ~/.ssh/id_ed25519.pub` to print out the contents of your public key. Copy the entire output of that command starting with ssh- to paste later.

### Add SSH key from Local to Linux Machine

Generating a public SSH key on your local machine and adding it to your ISU Coover Linux home directory will enable you to immedately connect to the Comparch machine without entering a password. 

**Windows:**
1. Generate SSH key with above steps on local machine
2. Copy the generated SSH key from local machine to Linux SSH machine: `scp C:\Users\username\.ssh\id_ed25519.pub comparch:~/.ssh/authorized_keys`
    - NOTE: If the `authorized_keys` file exists on the Linux machine already, copy the public key from the local machine into the file instead of copying a new file

NOTE: Change `username` to local machine Username on personal computer.

**MacOS/Linux:**
1. Generate SSH key with above steps on local machine
2. Copy the generated SSH key from local machine to Linux SSH machine: `ssh-copy-id comparch`

### Add SSH Key from Linux to GitLab Account

Generating a public SSH key on the Comparch machine and adding it to your ISU Gitlab account will enable you to automatically push/pull code without entering a password. 

1. Log into https://git.ece.iastate.edu/ and navigate to the account menu in the upper right corner of the toolbar, then to the `Settings` page in the dropdown menu.
2. In the sidebar on the settings page, select `SSH Keys`. Select New SSH Key to add your new key. This is also the page in which you may remove SSH keys you no longer use or have been compromised.
3. Paste the **entire contents** of the `id_ed25519.pub` public key file into this text box, enter a name to identify this computer, and press `Add SSH Key`.
4. Now copy your local SSH key over to comparch:
    - Open a terminal on your local device
    - Run the command scp ~/.ssh/id_ed25519 ~/.ssh/id_ed25519.pub comparch:~/.ssh
4. Your computer and comparch will now automatically use your SSH key to authenticate with GitLab when using the command line tools.

*In the future, when you clone a repository, there will always be at least two URLs provided. One will be labeled HTTPS and the other SSH. Use the SSH urls provided in each repository or git will ignore your SSH Key and ask for a password.*

## Connecting VSCode to Comparch

To connect to comparch via VSCode, first install VSCode from https://code.visualstudio.com.

1. Click the small `><` icon in the bottom left corner of the VSCode window.
2. Select `Connect to Host...`, install the Remote-SSH extension if prompted.
3. Select `comparch` in the list that appears. if `comparch` does not appear, ensure that `ssh comparch` works in a terminal or command prompt. Otherwise, check your ssh config again (see above)
4. A new VSCode window will appear, you can now open files and folders on the remote machine. To open a remote terminal, click `Terminal > New Terminal` in the menu bar.
