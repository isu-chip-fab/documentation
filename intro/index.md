# Introduction Index

Go through the following page in order to get required tools set up:
1. [New Members](new_members.md)

Additional tools, use as needed: 
- [Toolchain](toolchain.md)
- [Additional Tools](tools.md)

Supplementary Resources:
- [Git Commands](git.md)
- [Linux Commands](linux.md)