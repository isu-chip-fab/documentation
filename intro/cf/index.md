# ChipForge Command Line Utility

The `chipforge` command line utility is a collection of python tools commonly used by the club.
It includes the behavior of the old `fpgagen` utility as `chipforge fpga`.

`chipforge` is also installed as a shortened alias `cf`

## Install using pip3
```sh
pip3 install chipforge -U --user --index-url https://git.ece.iastate.edu/api/v4/projects/6561/packages/pypi/simple
```

## Firmware Utilities
... TODO

## FPGA Utilities
See [FPGA Utilities](./fpga.md)