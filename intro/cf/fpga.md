# Running Caravel projects on an FPGA

[What is an FPGA?](https://en.wikipedia.org/wiki/Field-programmable_gate_array) 

Running Caravel projects on an FPGA is a great way to experiment with digital designs without waiting for long simulations.

This tool deals with all the nuisance of setting up and using a Vivado project from the sources in a caravel user project.

See [https://git.ece.iastate.edu/isu-chip-fab/chiputil](https://git.ece.iastate.edu/isu-chip-fab/chiputil) for details.

## Limitations

This sets the `FUNCTIONAL` and `SIM` macros, so functional replacements are used as in simulation, so it will do a best
approximation of the final chip.

Analog and Pullup/down GPIO settings are unsupported. Modify the constraints .xdc file if those are needed.

***The generated Vivado project is considered temporary. It will be overwritten.*** Suggested usage is to only
to use the `chipforge fpga` command. 

If you wish to open the Vivado project directly, you may open the vivado project (see `VIVADO_PROJECT` in `fpga_config.ini`).
If you edit files in Vivado to debug, they will be overwritten by all `chipforge fpga` options except `chipforge fpga --template`.
Write a transform (see `chipforge fpga --template`) for any changes to the caravel core you wish to persist.

This has only been tested on RHEL 8 on ISU campus. Windows compatibility is possible, but unlikely.

## Install using pip3
This is a component of the larger `chipforge` utility, see [Chipforge CLI Installation](/intro/cf/index.md?id=install-using-pip3) for installation instructions.

## Simple Usage

With an Arty A7-100 FPGA plugged in on an ISU campus Linux machine, run:
```
cd caravel_user_project
chipforge fpga --flash
```

And it will generate a Vivado project and flash the FPGA.

## Detailed Usage

By default, running `chipforge fpga` in a `caravel_user_project` repository will create an FPGA project, copy and modify sources
as needed, and run Vivado to generate a bitstream. `--dry-run` will create the tcl script for Vivado, but not launch it.
Useful for debugging.

Alternate options are:
```sh
chipforge fpga --parse     # Only parse and write the confguration file
chipforge fpga --transform # Only copy and modify sources
chipforge fpga --bitstream # Generate a bitstream (default)
chipforge fpga --flash     # Generate bitstream and flash
chipforge fpga --template  # Set up the transform template for adding a new transform to modify source files
                  # This is how to modify the wrapper or xdc file, etc.
```

See the configuration file `fpga_config.ini` generated when running `chipforge fpga` for more information on configuration options including changing the default board and Vivado path.