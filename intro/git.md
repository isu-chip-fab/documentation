# Git Overview

## Cloning a Repository

Clone repository: `git clone REPO_URL`

Example: `git clone https://github.com/efabless/caravel_user_project.git`

NOTE: If you have a public SSH key from your local machine on your GitLab account, you can use the SSH clone and not need to log in!

## Pulling Changes

Check for/pull down remote updates: `git pull`

## Pushing Changes

From root of repo, add all changed files: `git add .`

Commit added changes: `git commit -m "COMMIT MESSAGE"`

Push changes to remote repository: `git push`

## Managing Branches

Check current branch: `git branch`

List all branches in repository: `git branch -r`

Switch to existing branch: `git checkout EXISTING_BRANCH_NAME`

Create new branch and switch to it: `git checkout -b NEW_BRANCH_NAME`

## Git Cheat Sheet

For a cheat sheet with common Git commands and their usage: https://education.github.com/git-cheat-sheet-education.pdf