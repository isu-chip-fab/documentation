# Additional Tools (As Needed)

The following tools and resources can be utilized for local work. Note that most work should typically be done on the comparch machine or a Linux VDI. 

## Mounting X Drive

The mounted Linux X Drive can be utilized to read/write work that is generated from the supported toolchain on the remote comparch computer or Linux VDI.

The following link will show you how to mount the Linux X Drive from your ISU Coover Linux home directory: https://etg.ece.iastate.edu/x-drive/

## eFabless Slack Channel

Slack Channel to discuss everything eFabless: https://join.slack.com/t/open-source-silicon/shared_invite/zt-1zopfd1gk-uI2eSlNXB54xN9RCowGa4g

This is very beneficial for "deeper" problems that can not be solved by senior members or by googling. 

## Verilog Code Formatter

In VSCode, there is a supported Verilog autoformatter extension titled `SystemVerilog and Verilog Formatter`. This can be installed by opening the `Extensions` tab by pressing `Ctrl+Shift+X`, and entering the name of the formatter extension. After this, select the extension and press the `install` button. 

To format Verilog code, right-click on the text window and select `Format Document`. The keybind for this is `Ctrl+Shift+I`. 

If this is the first time running the `Format Document` command, it is required to select the newly installed auto-formatter as the target formatting plugin. 