# Linux Commands

## Screen

Screen requires the toolchain to be activated. Reference [Toolchain](toolchain.md). 

Sessions can be used to run the design toolflow and then be detached from. This is beneficial since the toolflow can still be processed after a remote SSH connection is terminated. The following commands provide the basics for utilizing `screen` on Linux. 

Create new named session: `screen -s NAME`

Detach from session: `CTRL+a d`

Reattach to session: `screen -r NAME`

List all sessions: `screen -ls`

Before running a desired program, ensure you are attached to a session with the `screen -ls` command. After this, you may call your program, and then call `CTRL+a d` to detach from the session. After this, you can safely terminate the SSH connection while guaranteeing your program is running on the remote computer. 

For additional commands: `screen --help`

## Command Reference

For a cheat sheet with common Linux Bash commands: https://hpc.ua.edu/wp-content/uploads/2022/02/Linux_bash_cheat_sheet.pdf
