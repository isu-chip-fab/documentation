#!/bin/bash

# ChipForge Repository update script

# Simply call this periodically to update our local copies of the caravel user project!

# THIS WILL OVERWRITE THE repo_updater_caravel FOLDER IN THE CURRENT DIRECTORY WHILE WORKING

log() {
  echo -e "\n\033[32;1m*** $1 ***\033[0m\n"
}

upgrade() {
  set -e

  log "Cleaning up"
  rm -rf repo_updater_caravel

  log "Clone and set up remotes"

  EFABLESS_REMOTE=https://github.com/efabless/caravel_user_project
  CHIPFORGE_REMOTE=git@git.ece.iastate.edu:isu-chip-fab/caravel_user_project

  git clone --depth=1 -b main $CHIPFORGE_REMOTE repo_updater_caravel
  cd repo_updater_caravel

  git remote add efabless $EFABLESS_REMOTE
  git remote add chipforge $CHIPFORGE_REMOTE

  git fetch --depth=1 efabless main
  git fetch --depth=1 chipforge main efabless

  log "Check for changes from efabless/main"

  git checkout efabless/main # Checkout the remote files so our local files are updated
  EFABLESS_COMMIT=$(git rev-parse HEAD)
  git reset --soft chipforge/efabless # Reset our ref to be pointing to chipforge/efabless, but leave local files intact
  
  # https://stackoverflow.com/a/25149786
  if [[ `git status --porcelain --untracked-files=no` ]]; then
    # If file changes are found, commit and push them
    log "Changes found, pushing changes to chipforge/efabless and creating merge request"

    git add -A
    git commit -m "Upstream $EFABLESS_COMMIT"
    git push chipforge HEAD:efabless # Push direct to efabless branch and make merge request on separate branch so merge conflict fixes don't edit the efabless branch
    git push chipforge HEAD:refs/heads/efabless-$EFABLESS_COMMIT -o merge_request.create -o merge_request.target=main

    log "Use the merge request above to complete the merge into caravel_user_project to main, then rebase other branches as needed"
  else
    log "No changes found. Exiting"
    return 0
  fi
}

set -x
{
  (upgrade)
}