# Wiki Updates

If any technical information, toolchain, or processes change, refer to the following pages in each major section of the wiki to update. 

## Introduction

The introduction section may require updates dependening on lab resources, ETG links, or when the internal supported toolchain updates. 

* [Lab Access](/intro/new_members?id=Durham-310-access)
* [ETG VPN](/intro/new_members?id=off-campus-vpn)
* [SSH to Host Server](/intro/new_members?id=connecting-to-a-new-machine)
* [ETG VDI](/intro/getting_started?id=installing-vdi)
* [ETG X Drive](/intro/getting_started?id=mounting-x-drive)
* [Toolchain](/intro/toolchain)
* [Efabless Slack](/intro/reference?id=efabless-slack-channel)

## Bringup

This section of documentation will most likely need to be updated when ChipForge updates the supported hardware to evaluate chips, or when the software repository to build code for the management core updates. 

* [Caravel Board HW](/bringup/hw_guide)
* [Caravel Board SW Repo](/bringup/sw_guide)
* [Caravel Management Core Registers/Addresses](/bringup/management_core)
* [Course References](/bringup/reference?id=course-references)

## Caravel

The Caravel section summarizes the digital design process for the user area of a digital chip. This will require updates as makefile commands update, new efabless branches get made, or our process with the supported tools update. 

* [Caravel Board HW](/caravel/hw_guide)
* [Caravel Sources](/caravel/sim?id=setting-up-sources)
* [Simulation Design](/caravel/sim?id=including-verilog-design-files)
* [Simulation Commands](/caravel/sim?id=register-transfer-level-rtl-simulations)
* [GTKWave](/caravel/sim?id=verifying-waveforms)
* [Hardening Make Commands](/caravel/hardening?id=make-hardening)
* [KLayout](/caravel/hardening?id=klayout)
* [Hardening Design](caravel/hardening?id=new-hardening-config)
* [Precheck Make Commands](/caravel/precheck)
* [FPGA Flash](/caravel/fpga)

NOTE: Many of these updates are also included in the [Introduction](/caravel/introduction) page, which may also need to be updated for make commands for simulation or hardening. Changes may also may be needed for the [Caravel Design Tutorials](/tutorial/index) pages which run through the caravel process. 

## Analog

The Caravel section summarizes the analog design process for the user area of an analog chip. This will require updates as makefile commands update, new efabless branches get made, or our process with the supported tools update. 

* [Analog Repo Build (New Toolflow?)](/analog/building)
* [Source Enviornment Variables](/analog/schematic?id=source-enviornment-variables)
* [XSchem and Schematics](/analog/schematic?id=starting-xschem)
* [Magic and Layout](/analog/layout)
* [Netgen and LVS](/analog/LVS)
* [ReRAM](/analog/reram)

NOTE: Many of these updates are also included in the [Introduction](/analog/introduction) page, which may also need to be updated for make commands for simulation or hardening.


## Tutorial

Maintnence for these sections may require updates to the corresponding tutorials git repo inside of the ChipForge git repo. 

* [Adder Design](/tutorial/adder)
* [OpenRAM Design](/tutorial/openram)

## Standards

The Coding Standards section will need to be updated when any new rules are set to follow a specific structure for designing either software or hardware within ChipForge. 

* [C Standards](/standard/c)
* [Verilog Standards](/standard/verilog)
* [File Naming](/standard/file_name)
* [Git](/standard/git)
