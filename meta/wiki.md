# Wiki Page

## Docsify Setup

1. Visit this link to install npm: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

2. Install required dependencies with npm:
```bash
npm i docsify-cli -g
```

## Preview Website Locally

```bash
docsify serve
```

The locally hosted site for the current branch can be viewed at `http://localhost:3000`

## Repo Overview

These unique files perform the following purpose:
- `README.md`: Landing page of the website
- `_sidebar.md`: Determines links to markdown files in lefthand sidebar of wiki
- `index.html`: Holds scripts for Docsify to build, can change plugins under `window.$docsify` script
- `.nojekyll`: Tells Gitlab Pages to not run published files through Jekyll, generated from Docsify
- `.gitlab-ci.yml`: CI/CD file to generate Gitlab Pages artifact for hosting. Will run on every push and update automatically
- `_redirects`: Gitlab Pages configuration file to set index.html as the default route

The `\img\` directory contains all pictures for each markdown page, seperated by file name of each markdown page. 

## Hosting

This website is hosted on the Iowa State ECPE Gitlab servers, under the Group and Repository `ChipForge/Documentation`. 

The CI/CD runner is activated with the file `.gitlab-ci.yml` in the root directory of the repository. This script will generate a public artifact of the website contents and deploy it using Gitlab Pages at the URL https://git-pages.ece.iastate.edu/isu-chip-fab/documentation/. 

The Gitlab Pages tab can be accessed from the repository under `Deploy\Pages`. The deployment status can be monitored under `Build\Jobs` from the repository.

Documentation Gitlab Repo: https://git.ece.iastate.edu/isu-chip-fab/documentation

## Troubleshooting

### 404 not found when visiting a direct page
Using history mode, all not found requests must return the index.html file instead of the default 404 page.
This can be configured using an htaccess file for apache servers, nginx configuration files, or similar depending on the
web host settings. If this is not possible, edit index.html and unset history routing mode.

If you see this in development, check that your docsify version `docsify --version` is >= 4.4.4. If it is not, run `npm -g update` and try again.

### Absolute Paths

When hosting on Gitlab pages, it is required that every internal link to other web pages or links to images have absolute paths. If any markdown file or image has a `/` before its path or filename, the website will not be able to load with Gitlab Pages.

## Resources

- Docsify Introduction: https://docsify.js.org/#/quickstart

## TikzJax
```
Hint: Use github codespaces

https://github.com/artisticat1/tikzjax/tree/output-single-file

apt install texlive fontforge
export NODE_OPTIONS=--openssl-legacy-provider
npm i
npm run build
```
Copy files from dist into the root of this repository