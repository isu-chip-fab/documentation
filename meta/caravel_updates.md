# Updating the Caravel User Project Template

Use the shell script at [repo\_updater.sh](https://git.ece.iastate.edu/isu-chip-fab/documentation/-/blob/main/meta/repo_updater.sh?ref_type=heads) to create a merge request to update the main branch of the chipforge `caravel_user_project` repository. Then proceed to rebase all derivative branches based on that main branch. 

This script ensures we don't have the full efabless commit history in our repositories, but only a single commit from each time we pull in major changes.

*** All Caravel User Project repositories should be based on our template's main branch, _not_ eFabless's as our main branch has updates required for our firmware environment and library usage. ***
