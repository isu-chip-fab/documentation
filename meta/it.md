# IT Documentation

## Addition of New Members

1) Add the member to MS Teams and https://git.ece.iastate.edu/isu-chip-fab
2) Add the member to the stuorg via https://www.stuorg.iastate.edu/organizations/???
3) Add the member to the ASW listman group via ???

## Leadership Addition

1) Add them as an officer of the stuorg via https://www.stuorg.iastate.edu/organizations/???
2) Add them as an admin of the ASW listman group
3) Add them as an owner of https://git.ece.iastate.edu/isu-chip-fab

## Leadership Removal

1) Remove them as an officer of the stuorg via https://www.stuorg.iastate.edu/organizations/???
2) Remove them as an admin of the ASW listman group
3) Remove them as an owner of https://git.ece.iastate.edu/isu-chip-fab

Optional:
* Provide them access on a personal email to MS Teams and/or git.ece.iastate.edu so they can be reached for questions and reference their old projects.