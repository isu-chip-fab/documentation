<!-- /sidebar.md -->

[Standards](index.md)

* [C Standards](c.md)
* [Verilog Standards](verilog.md)
* [File Naming](file_name.md)
* [Git](git.md)