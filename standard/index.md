# Standard Index

The following list of pages will outline expected standards for each part of the design process:
* [C Standards](c.md)
* [Verilog Standards](verilog.md)
* [File Naming](file_name.md)
* [Git](git.md)