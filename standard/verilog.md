# Verilog Coding Standards

Inspiration: https://github.com/lowRISC/style-guides/blob/master/VerilogCodingStyle.md

## Signal Naming
### Port Suffixes

Ports for a module should include the following prefix based on each type:
- Input: `_i`
- Output: `_o`
- Inout: `_io`

GOOD: 
```verilog
module DESIGN(input D0_i, 
              input D1_i, 
              input S_i, 
              inout D_io,
              output X_o);
```

BAD:  
```verilog
module DESIGN(input D0, 
              input D1, 
              input S, 
              inout D,
              output X);
```

### Signal Prefixes

The following related signals should include prefixes:
- Wishbone Bus: `wbs_`
- Logic Analyzer Pins: `la_`
- GPIO Pins: `io_`


## Port Lists
### Define Port Type Inside Port List

Port types, including `input`, `output`, and `inout` should be defined within the port list of a module, not after it.

GOOD: 
```verilog
module MUX (input D0_i, 
            input D1_i, 
            input S_i, 
            output X_o);
```

BAD:  
```verilog
module MUX(D0_i, 
           D1_i, 
           S_i, 
           X_o);
input D0_i, D1_i, S_i;
output X_o;
```

### One Port Per Line

One port should be defined per line for module definition. 

GOOD: 
```verilog
module MUX (input D0_i, 
            input D1_i, 
            input S_i, 
            output X_o);
```

BAD:  
```verilog
module MUX(input D0_i, input D1_i, input S_i, output X_o);
```

### No Implicit Definitions

Positional definition of signals in module instantation should not be used. Instead, specify each signal directly. 

GOOD: 
```verilog
MUX g_MUX(
    .D0_i(D0_s),
    .D1_i(D1_s),
    .S_i(S_s),
    .X_o(X_s));
```

BAD:
```verilog
MUX g_MUX(
    D0_s,
    D1_s,
    S_s,
    X_s);
```